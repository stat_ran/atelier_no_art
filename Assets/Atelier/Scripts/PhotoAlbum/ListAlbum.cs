using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Atelier.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ListAlbum : BaseBehavior
{

    public DepartmentType DepartmentType;
    
    private SpriteRenderer[] _sprite;
    private List<PhotoInAlbum> _photoInAlbums;
    [HideInInspector] public PhotoButton PhotoButton;

    public override void Start()
    {
        base.Start();
    }

    public void Init()
    {
        _photoInAlbums = GetComponentsInChildren<PhotoInAlbum>(true).ToList();
        PhotoButton = GetComponentInChildren<PhotoButton>(true);
        PhotoButton.Init();
    }

    public void ChangeLayer(string layer)
    {
        foreach (var spriteRenderer in _sprite)
        {
            spriteRenderer.sortingLayerName = layer;
        }
    }

    public void InitPhoto()
    {
        foreach (var album in _photoInAlbums)
        {
            album.Init();
        }
        
        List<string> prefabs = GC.GetSavedDepartment(DepartmentType);
        _sprite = GetComponentsInChildren<SpriteRenderer>(true);
        ChangeLayer(_sprite[0].sortingLayerName);
        if (prefabs==null) return;
        
        foreach (var prefab in prefabs)
        {
            Debug.Log(name + " " + prefab);
        }
        
        int i = prefabs.Count - 1;
        foreach (var photoInAlbum in _photoInAlbums)
        {
            photoInAlbum.SetPhoto(DepartmentType, prefabs[i]);
            i--;
            if (i < 0)
            {
                _sprite = GetComponentsInChildren<SpriteRenderer>(true);
                ChangeLayer(_sprite[0].sortingLayerName);
                return;
            }
        }
        _sprite = GetComponentsInChildren<SpriteRenderer>(true);
        ChangeLayer(_sprite[0].sortingLayerName);
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(DepartmentType.ToString());
    }
}
