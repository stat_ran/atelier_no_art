using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;

public class PhotoInAlbum : BaseBehavior
{
    [HideInInspector] public GameObject Prefab;
    public GameObject Fon;
    public GameObject Fon_Clear;
    public Transform PrefabPoint;

    public override void Start()
    {
        base.Start();
    }

    public void Init()
    {
        Fon.SetActive(false);
        Fon_Clear.SetActive(true);
    }

    public void SetPhoto(DepartmentType type, string namePhoto)
    {
        Debug.Log("SetPhoto");
        Fon_Clear.SetActive(false);
        Fon.SetActive(true);
        Prefab = Instantiate(GC.GetSavedPrefab(type, namePhoto), PrefabPoint);
        Prefab.name = namePhoto;
        Prefab.SetActive(true);
        Prefab.transform.localPosition=Vector3.zero;
        Prefab.GetComponent<BaseClothesPrefab>().Load();
    }
}
