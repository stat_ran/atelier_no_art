using System.Collections.Generic;
using System.Linq;
using Atelier.Scripts;
using DG.Tweening;
using UnityEngine;

public class PhotoAlbum : BaseBehavior
{
    public GameObject PhotoPrefab;
    public GameObject Parent;
    public GameObject UI_Album;
    public bool IsMove;
    
    private List<ListAlbum> _listAlbums;
    private int _upList = 0;
    private float _speedChange=0.3f;

    private Vector2 _middle;
    private Vector2 _left1;
    private Vector2 _right1;
    private Vector2 _left2;
    private Vector2 _right2;
    private Vector3 _scale;


    public override void Awake()
    {
        base.Awake();
    }

    public override void Start()
    {
        Debug.Log("Start");
        base.Start();
        Parent.SetActive(false);
        UI_Album.SetActive(false);
        _listAlbums = GetComponentsInChildren<ListAlbum>(true).ToList();
        foreach (var listAlbum in _listAlbums)
        {
            listAlbum.gameObject.SetActive(true);
            listAlbum.Init();
        }
        _middle = _listAlbums[0].transform.position;
        _scale = _listAlbums[0].transform.localScale;
        _left1 = _listAlbums[1].transform.position;
        _left2 = _listAlbums[2].transform.position;
        _right1 = _listAlbums[_listAlbums.Count - 1].transform.position;
        _right2 = _listAlbums[_listAlbums.Count - 2].transform.position;
        
    }

    public void OpenAlbum()
    {
        Parent.SetActive(true);
        UI_Album.SetActive(true);
        _listAlbums[0].PhotoButton.Collider.enabled = true;
        InitPhoto();
        NotificationCenter.postNotification(Notifications.Open_PhotoAlbom, null);
    }

    public void CloseAlbum()
    {
        Parent.SetActive(false);
        UI_Album.SetActive(false);
        NotificationCenter.postNotification(Notifications.Close_PhotoAlbom, null);
    }

    private void InitPhoto()
    {
        foreach (var listAlbum in _listAlbums)
        {
            listAlbum.InitPhoto();
        }
    }

    public void Move(float delta)
    {
        if (GetList(_upList).transform.localPosition.x + delta > 0)
        {
            GetList(_upList).transform.Translate(Vector2.right*delta);
            GetList(_upList+1).transform.Translate(Vector2.right*delta);
        }
        else
        {
            GetList(_upList).transform.Translate(Vector2.right*delta);
            GetList(_upList-1).transform.Translate(Vector2.right*delta);
        }
    }

    private ListAlbum GetList(int number)
    {
        if (number < 0) return _listAlbums[_listAlbums.Count - 1];
        if (number >= _listAlbums.Count) return _listAlbums[0];
        return _listAlbums[number];
    }

    public void CreateDepartment(DepartmentType departmentType)
    {
        if (PlayerPrefs.HasKey(departmentType.ToString()))
        {
            DepartmentSavePrefs departmentSavePrefs =
                JsonUtility.FromJson<DepartmentSavePrefs>(PlayerPrefs.GetString(departmentType.ToString()));
            foreach (var pref in departmentSavePrefs.PrefabsName)
            {
                foreach (var prefab in Data.Scarf.Prefabs)
                {
                    if (pref == prefab.name)
                    {
                        CreatePhoto(prefab.gameObject);
                    }
                }
            }       
        }
    }

    private void CreatePhoto(GameObject clothePrefab)
    {
        GameObject photo = Instantiate(PhotoPrefab);
        photo.transform.position = transform.position;
        GameObject clothe = Instantiate(clothePrefab, photo.transform);
        clothe.name = clothePrefab.name;
        clothe.transform.localPosition = Vector3.zero;
        clothe.GetComponent<BaseClothesPrefab>().Load();
        clothe.SetActive(true);
    }
    
    private Vector2 _lastPosition;
    private Vector2 _currentPosition;
    
    private void OnMouseDown()
    {
        // return;
        // IsMove = true;
        // _lastPosition = MousePosition;
        // _currentPosition = MousePosition;
    }

    private void OnMouseDrag()
    {
        // return;
        // _currentPosition = MousePosition;
        // Move(_currentPosition.x-_lastPosition.x);
        // _lastPosition = _currentPosition;
    }

    private void OnMouseUp()
    {
        // return;
        // IsMove = false;
    }

    public void LeftButton()
    {
        if(IsMove) return;
        IsMove = true;
        ChangeList(false);
    }

    public void RightButton()
    {
        if(IsMove) return;
        IsMove = true;
        ChangeList(true);
    }

    private void ChangeList(bool isRight)
    {
        if (isRight)
        {
            GetList(_upList+1).transform.DOScale(_scale, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList+1).transform.DOMove(_left1/2f, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList).transform.DOScale(_scale * 0.95f, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList).transform.DOMove(_right1/2f, _speedChange/2f).SetEase(Ease.Linear).onComplete += () => ContinueMove(isRight);
        }
        else
        {
            GetList(_upList-1).transform.DOScale(_scale, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList-1).transform.DOMove(_right1/2f, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList).transform.DOScale(_scale * 0.95f, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList).transform.DOMove(_left1/2f, _speedChange/2f).SetEase(Ease.Linear).onComplete += () => ContinueMove(isRight);
        }
    }

    private void ContinueMove(bool isRight)
    {
        
        if (isRight)
        {
            GetList(_upList + 1).transform.DOKill();
            GetList(_upList).transform.DOKill();
            
            GetList(_upList+1).ChangeLayer("List0");
            GetList(_upList+1).transform.DOScale(_scale, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList+1).transform.DOMove(_middle, _speedChange/2f).SetEase(Ease.Linear).onComplete += () => CompleteChange(isRight);
            GetList(_upList).transform.DOScale(_scale * 0.9f, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList).transform.DOMove(_right1, _speedChange/2f).SetEase(Ease.Linear);
        }
        else
        {
            GetList(_upList - 1).transform.DOKill();
            GetList(_upList).transform.DOKill();
            
            GetList(_upList-1).ChangeLayer("List0");
            GetList(_upList-1).transform.DOScale(_scale, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList-1).transform.DOMove(_middle, _speedChange/2f).SetEase(Ease.Linear).onComplete += () => CompleteChange(isRight);
            GetList(_upList).transform.DOScale(_scale * 0.9f, _speedChange/2f).SetEase(Ease.Linear);
            GetList(_upList).transform.DOMove(_left1, _speedChange/2f).SetEase(Ease.Linear);
        }
    }


    private void CompleteChange(bool isRight)
    {
        ChangeNumber(isRight);

        GetList(_upList).transform.DOKill();
        GetList(_upList).transform.position = _middle;
        GetList(_upList).transform.localScale = _scale;
        GetList(_upList).ChangeLayer("List1");
        GetList(_upList).PhotoButton.Collider.enabled = true;

        GetList(GetNumber(_upList + 1)).transform.DOKill();
        GetList(GetNumber(_upList + 1)).transform.position = _left1;
        GetList(GetNumber(_upList + 1)).transform.localScale = _scale * 0.9f;
        GetList(GetNumber(_upList + 1)).ChangeLayer("List2");
        GetList(GetNumber(_upList + 1)).PhotoButton.Collider.enabled = false;

        GetList(GetNumber(_upList + 2)).transform.DOKill();
        GetList(GetNumber(_upList + 2)).transform.position = _left2;
        GetList(GetNumber(_upList + 2)).transform.localScale = _scale * 0.8f;
        GetList(GetNumber(_upList + 2)).ChangeLayer("List3");
        GetList(GetNumber(_upList + 2)).PhotoButton.Collider.enabled = false;

        GetList(GetNumber(_upList + 3)).transform.DOKill();
        GetList(GetNumber(_upList + 3)).transform.position = _left2;
        GetList(GetNumber(_upList + 3)).transform.localScale = _scale * 0.8f;
        GetList(GetNumber(_upList + 3)).ChangeLayer("List4");
        GetList(GetNumber(_upList + 3)).PhotoButton.Collider.enabled = false;

        GetList(GetNumber(_upList + 4)).transform.DOKill();
        GetList(GetNumber(_upList + 4)).transform.position = _right2;
        GetList(GetNumber(_upList + 4)).transform.localScale = _scale * 0.8f;
        GetList(GetNumber(_upList + 4)).ChangeLayer("List4");
        GetList(GetNumber(_upList + 4)).PhotoButton.Collider.enabled = false;

        GetList(GetNumber(_upList + 5)).transform.DOKill();
        GetList(GetNumber(_upList + 5)).transform.position = _right2;
        GetList(GetNumber(_upList + 5)).transform.localScale = _scale * 0.8f;
        GetList(GetNumber(_upList + 5)).ChangeLayer("List3");
        GetList(GetNumber(_upList + 5)).PhotoButton.Collider.enabled = false;

        GetList(GetNumber(_upList + 6)).transform.DOKill();
        GetList(GetNumber(_upList + 6)).transform.position = _right1;
        GetList(GetNumber(_upList + 6)).transform.localScale = _scale * 0.9f;
        GetList(GetNumber(_upList + 6)).ChangeLayer("List2");
        GetList(GetNumber(_upList + 6)).PhotoButton.Collider.enabled = false;

        IsMove = false;
    }

    private int GetNumber(int num)
    {
        if (num >= _listAlbums.Count) return num -= _listAlbums.Count;
        return num;
    }

    private void ChangeNumber(bool isRight)
    {
        if (isRight) _upList++;
        else _upList--;
        if (_upList >= _listAlbums.Count) _upList = 0;
        if (_upList < 0) _upList = _listAlbums.Count - 1;
    }

}
