using Atelier.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhotoButton : BaseBehavior
{

    [HideInInspector] public BoxCollider2D Collider;
    
    private ListAlbum _listAlbum;

    public override void Start()
    {
        base.Start();
    }

    public void Init()
    {
        _listAlbum = GetComponentInParent<ListAlbum>();
        Collider = GetComponent<BoxCollider2D>();
        Collider.enabled = false;
    }

    private void OnMouseDown()
    {
        _listAlbum = GetComponentInParent<ListAlbum>();
        SceneManager.LoadScene(_listAlbum.DepartmentType.ToString());
    }
}
