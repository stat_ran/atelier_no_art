﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoesPrefab : BaseClothesPrefab
{

    public SpriteRenderer Sole;
    
    public override void Awake()
    {
        base.Awake();
        DepartmentType = DepartmentType.Shoes;
    }

    public void SetDecor()
    {
        
    }

}
