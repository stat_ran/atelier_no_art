﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Платья. Декор - Пояса!
public class DressPrefab : BaseClothesPrefab
{

    public override void Awake()
    {
        base.Awake();
        DepartmentType = DepartmentType.Dress;
    }

    public override void Save()
    {
        base.Save();
        SavePrefab();
        PlayerPrefs.SetString(name +"_Decor1", DecorsPoints[0].SpriteRendererPositions[0].sprite.name);
        PlayerPrefs.SetInt(name+"_Color", ColorForPrefab.Number);
        PlayerPrefs.Save();
    }

    public override void Load()
    {
        base.Load();
        string str = PlayerPrefs.GetString(name + "_Decor1");
        
        foreach (var decor in Data.Departments[DepartmentType].Decorses[0].Decors)
        {
            if (decor.Sprites[0].name == str)
            {
                DecorsPoints[0].SpriteRendererPositions[0].sprite = decor.Sprites[0];
            }
        }
        
        SetColor(PlayerPrefs.GetInt(name + "_Color"));

    }

}
