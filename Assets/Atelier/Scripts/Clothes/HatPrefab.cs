﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Шапки. Декор - Один на лоб. Второй Уши из двух спрайтов!
public class HatPrefab : BaseClothesPrefab
{

    public Color[] Colors;
    public SpriteRenderer DecorUzor;
    
    public override void Awake()
    {
        base.Awake();
        DepartmentType = DepartmentType.Hat;
        DecorUzor.enabled = false;
    }

    public override void Save()
    {
        base.Save();
        SavePrefab();
        PlayerPrefs.SetString(name +"_Decor1", DecorsPoints[0].SpriteRendererPositions[0].sprite.name);
        PlayerPrefs.Save();
    }

    public override void Load()
    {
        base.Load();
        string str = PlayerPrefs.GetString(name + "_Decor1");
        
        foreach (var decor in Data.Departments[DepartmentType].Decorses[0].Decors)
        {
            if (decor.Sprites[0].name == str)
            {
                DecorsPoints[0].SpriteRendererPositions[0].sprite = decor.Sprites[0];
            }
        }

        DecorUzor.enabled = true;

    }
}
