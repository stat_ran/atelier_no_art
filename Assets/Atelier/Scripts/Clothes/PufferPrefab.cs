﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PufferPrefab : BaseClothesPrefab
{

   public override void Awake()
   {
      base.Awake();
      DepartmentType = DepartmentType.Puffer;
   }

   public override void Save()
   {
      base.Save();
      SavePrefab();
      PlayerPrefs.SetString(name +"_Decor1", DecorsPoints[0].SpriteRendererPositions[0].sprite.name);
      PlayerPrefs.SetString(name +"_Decor2", DecorsPoints[1].SpriteRendererPositions[0].sprite.name);
      PlayerPrefs.SetInt(name+"_Color", ColorForPrefab.Number);
      PlayerPrefs.Save();
   }

   public override void Load()
   {
      base.Load();
      string str1 = PlayerPrefs.GetString(name + "_Decor1");
      string str2 = PlayerPrefs.GetString(name + "_Decor2");
      
      foreach (var decor in Data.Departments[DepartmentType].Decorses[0].Decors)
      {
         if (decor.Sprites[0].name == str1)
         {
            DecorsPoints[0].SpriteRendererPositions[0].sprite = decor.Sprites[0];
            DecorsPoints[0].SpriteRendererPositions[1].sprite = decor.Sprites[0];
            DecorsPoints[0].SpriteRendererPositions[2].sprite = decor.Sprites[0];
         }
      }
      
      foreach (var decor in Data.Departments[DepartmentType].Decorses[1].Decors)
      {
         if (decor.Sprites[0].name == str2)
         {
            DecorsPoints[1].SpriteRendererPositions[0].sprite = decor.Sprites[0];
            DecorsPoints[1].SpriteRendererPositions[1].sprite = decor.Sprites[1];
            
         }
      }
      
      SetColor(PlayerPrefs.GetInt(name + "_Color"));
      
   }
   
   
   
}
