﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AceCream.Scripts.Template.Extensions
{
    public static class ListExtension
    {
        public static bool IsNullOrEmpty<T>(this List<T> list)
        {
            return list == null || list.Count <= 0;
        }
        
        public static bool IsNullOrEmpty<T>(this T[] list)
        {
            return list == null || list.Length <= 0;
        }
        
        public static bool IsOutOfRange<T>(this T[] array, int index)
        {
            if (array.IsNullOrEmpty()) return true;
            return index < 0 || index >= array.Length;
        }
        
        public static T GetRandom<T>(this T[] array)
        {
            return array[Random.Range(0, array.Length)];
        }

        public static bool NotNull<T>(this T @object) where T : UnityEngine.Object
        {
            return !@object.IsNull();
        }

        public static bool IsNull<T>(this T @object) where T : UnityEngine.Object
        {
            return @object == null;
        }
    }
}