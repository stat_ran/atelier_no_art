﻿public interface IStage
{
    void Init(BaseController baseController);
    void StartStage();
    void CompleteStage();
    void KillStage();
}
