﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Atelier.Scripts
{
   public class MainMenuController : BaseBehavior
   {
      public List<Pers> Perses;
      public Transform StartPoint;
      public Transform EndPoint;
      public GameObject ChangeDesc;
      

      //ДЛя ФотоРамки
      public GameObject PhotoFrame;
      public Transform PrefabPoint;
      public Transform EndPoint_PhotoFrame;

      private AnimationCurve _curve = AnimationCurve.Linear(0, 0, 1, 1);

      public DepartIcon[] DepartIcons;

      public override void Start()
      {
         base.Start();
         Debug.Log(GC.NumberPers + " " + GC.IsBuy);
         NotificationCenter.addCallback(Notifications.End_Dialog, StartChangeDesc);
         ChangeDesc.SetActive(false);
         InitPerses();
         StartPers();
         PhotoMove();
      }
      
      private void OnDestroy()
      {
         NotificationCenter.removeCallbeck(Notifications.End_Dialog, StartChangeDesc);
      }

      #region PhotoFrame

      private void PhotoMove()
      {
         PhotoFrame.SetActive(false);
         
         if(GC.NamePrefab_PhotoFrame=="") return;

         PhotoFrame.SetActive(true);
         GameObject prefab = Instantiate(GC.GetSavedPrefab(GC.DepartmentType_Photo, GC.NamePrefab_PhotoFrame));
         prefab.name = GC.NamePrefab_PhotoFrame;
         prefab.transform.parent = PrefabPoint;
         prefab.transform.localPosition=Vector3.zero;
         prefab.transform.localScale=Vector3.one;
         prefab.SetActive(true);
         prefab.GetComponent<BaseClothesPrefab>().Load();
         
         PhotoFrame.transform.DOMove(EndPoint_PhotoFrame.position, 1f).SetEase(Ease.Linear).onComplete += PhotoFinish;
         PhotoFrame.transform.DOScale(PhotoFrame.transform.localScale / 4f, 1f).SetEase(Ease.Linear);
         
         GC.NamePrefab_PhotoFrame = "";
      }

      private void PhotoFinish()
      {
         PhotoFrame.transform.DOKill();
         Destroy(PhotoFrame);
      }

      #endregion
      

      private void InitPerses()
      {
         GC.PersCurrent = Perses[0];
      }
      
      private void Speek()
      {
         GC.PersCurrent.Animator.Play(AnimName.Hi.ToString());
      }

      public void StartPers()
      {
         if (GC.NumberPers < 0) GC.PersCurrent = Perses[Random.Range(0, Perses.Count)];
         else GC.PersCurrent = Perses[GC.NumberPers];

         GC.PersCurrent.Init(GetDepartment(), GC.IsBuy);
         //GC.PersCurrent.transform.position = StartPoint.position;
         //GC.PersCurrent.transform.DOMove(EndPoint.position, 3f).SetEase(_curve).onComplete+=Speek;
         //GC.PersCurrent.Animator.Play(AnimName.Walk_Start.ToString());
      }

      private DepartmentType GetDepartment()
      {
         int ran = Random.Range(0, Enum.GetNames(typeof(DepartmentType)).Length);
         return (DepartmentType)Enum.Parse(typeof(DepartmentType),  Enum.GetNames(typeof(DepartmentType))[ran], true);
      }

      private void StartChangeDesc(object obj)
      {
         ChangeDesc.SetActive(true);
      }

   }
   
   [System.Serializable]
   public class DepartIcon
   {
      public DepartmentType DepartmentType;
      public Sprite[] Icons;
   }
}
