using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersFass : MonoBehaviour
{
    public void CloudStart() //Запускается в анимации
    {
        GetComponentInParent<Pers>().CloudInit();
    }

    public void FinishGlad()
    {
        GetComponentInParent<Pers>().MoveToStart(AnimName.Walk_Buy);
    }
    
    public void FinishSad()
    {
        GetComponentInParent<Pers>().MoveToStart(AnimName.Walk_Sad);
    }
}
