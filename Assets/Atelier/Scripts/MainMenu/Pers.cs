﻿using System;
using System.Collections;
using System.Collections.Generic;
using AceCream.Scripts;
using Atelier.Scripts;
using DG.Tweening;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class Pers : BaseBehavior
{

    public GameObject Cloud;
    public SpriteRenderer ItemsPanel;
    public Animator AnimProfil;
    public Animator AnimFass;
    public int Number;
    
    
    [HideInInspector] public Animator Animator;
    private float _startScale=1f;
    private float _endScale = 1.4f;

    private float _speed = 5f; 
    private SpriteRenderer _spriteRenderer;
    private AnimationCurve _curve = AnimationCurve.Linear(0, 0, 1f, 1f);
    private DepartmentType _departmentType;
    private MainMenuController _controller;
    private Vector3 _vector3 = new Vector3(1f, 1f, 1f);
    private bool _isBuy;

    public override void Awake()
    {
        base.Awake();
        Animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _controller = FindObjectOfType<MainMenuController>();
        NotificationCenter.addCallback(Notifications.Open_PhotoAlbom, OpenAlbum);
        NotificationCenter.addCallback(Notifications.Close_PhotoAlbom, CloseAlbum);
    }

    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.Open_PhotoAlbom, OpenAlbum);
        NotificationCenter.removeCallbeck(Notifications.Close_PhotoAlbom, CloseAlbum);
    }

    private void OpenAlbum(object obj)
    {
        transform.DOPause();
    }
    
    private void CloseAlbum(object obj)
    {
        transform.DOPlay();
    }

    public void Init(DepartmentType departmentType, bool isBuy)
    {
        _departmentType = departmentType;
        Cloud.SetActive(false);
        _isBuy = isBuy;
        if(GC.NumberPers>=0) FinishAnim();
        else MoveToEnd();
        GC.NumberPers = Number;
    }

    public void MoveToEnd()
    {
        Debug.Log("MoveToEnd");
        AnimFass.gameObject.SetActive(false);
        transform.position = _controller.StartPoint.position;
        transform.localScale=Vector3.one;
        transform.DOMove(_controller.EndPoint.position, _speed).SetEase(Ease.Linear).onComplete+=StartShoping;
        transform.DOScale(Vector3.one * _endScale, _speed).SetEase(_curve);
        AnimProfil.Play(AnimName.Walk_Start.ToString());
    }

    private void FinishAnim()
    {
        AnimProfil.gameObject.SetActive(false);
        AnimFass.gameObject.SetActive(true);
        Cloud.SetActive(false);
        transform.localScale = _endScale * _startScale * Vector3.one;
        _vector3.x = -transform.localScale.x;
        _vector3.y = transform.localScale.y;
        _vector3.z = transform.localScale.z;
        transform.localScale = _vector3;
        _vector3.x = -_startScale;
        _vector3.y = _startScale;
        _vector3.z = _startScale;
        transform.position = _controller.EndPoint.position;
        AnimFass.Play(GC.IsBuy ? AnimName.Glad.ToString() : AnimName.Sad.ToString());
    }

    public void MoveToStart(AnimName animName)
    {
        AnimFass.gameObject.SetActive(false);
        AnimProfil.gameObject.SetActive(true);
        transform.DOMove(_controller.StartPoint.position, _speed).SetEase(_curve).onComplete+=StartNewPet;
        transform.DOScale(_vector3, _speed).SetEase(_curve);
        AnimProfil.Play(animName.ToString());
    }

    private void StartNewPet()
    {
        transform.DOKill();
        GC.IsBuy = false;
        GC.NumberPers = -1;
        _controller.StartPers();
    }

    public void FlipAnimPers()
    {
        if (Random.Range(0, 2) == 1)
        {
            _vector3.x = -transform.localScale.x;
            _vector3.y = transform.localScale.y;
            transform.localScale = _vector3;
        }
        else
        {
            _vector3.x = transform.localScale.x;
            _vector3.y = transform.localScale.y;
            transform.localScale = _vector3;
        }
    }

    void StartShoping()
    {
        AnimProfil.gameObject.SetActive(false);
        AnimFass.gameObject.SetActive(true);
        AnimFass.Play(AnimName.Hi.ToString());
    }

    public void CloudInit()
    {
        Cloud.SetActive(true);
        StartCoroutine(ChangeItem());
    }

    IEnumerator ChangeItem()
    {
        DepartIcon departIcon = _controller.DepartIcons[Random.Range(0, _controller.DepartIcons.Length)];
        Debug.Log(departIcon.DepartmentType);
        float t = Time.deltaTime;
        int numberSprite = 0;
        while (true)
        {
            yield return new WaitForSeconds(t);
            ItemsPanel.sprite = departIcon.Icons[numberSprite];
            numberSprite++;
            if (numberSprite >= departIcon.Icons.Length) numberSprite = 0;
            t += Time.deltaTime;
            if(t>=0.2f) break;
        }
        yield return new WaitForSeconds(1f);
        GC.StartScene(departIcon.DepartmentType);
        //MoveToStart(AnimName.Walk_Sad);
    }

}
