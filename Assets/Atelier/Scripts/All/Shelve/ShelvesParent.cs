﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Atelier.Scripts;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

public class ShelvesParent : BaseBehavior, IChildStage
{
    
    public GameObject ShelvePrefab;
    public Transform BoxUp;
    public Transform BoxDown;
    public Transform TargetClose;
    [HideInInspector] public GameObject CurrentDecor;
    [HideInInspector] public BaseController Controller;
    [HideInInspector] public DepartmentType Type;
    public int NumberDecor;
    public bool IsColor;

    private float _speed = 1f;
    private List<Shelve> _shelves = new List<Shelve>();
    private bool _isClick;
    private Vector2 _mousePosition;
    private float _deltaDist;
    private float _height;
    private Vector2 _startBoxUpPosition;
    private bool _isActive;
   

    public override void Awake()
    {
        base.Awake();
        Controller = FindObjectOfType<BaseController>();
    }

    public override void Start()
    {
        base.Start();
        Type = Controller.Type;
        transform.position = DownRight+Vector2.right*10f;
        _startBoxUpPosition = BoxUp.localPosition;
        BoxUp.localPosition = TargetClose.localPosition;
        if (!IsColor)
        {
            foreach (var decor in Data.Departments[Type].Decorses[NumberDecor].Decors)
            {
                Shelve shelve = Instantiate(ShelvePrefab, transform).GetComponentInChildren<Shelve>();
                shelve.Init(decor);
                _shelves.Add(shelve);
            }
        }
        else
        {
            foreach (var color in Data.Colors)
            {
                Shelve shelve = Instantiate(ShelvePrefab, transform).GetComponentInChildren<Shelve>();
                shelve.Init(color, Data.Departments[Type].ColorSprite);
                _shelves.Add(shelve);
            }
        }

        Vector2 startPoint=new Vector2(BoxDown.position.x, UpRight.y);
        float dist = 0;
        foreach (var shelf in _shelves)
        {
            shelf.TransformParent.position = startPoint + Vector2.down * dist;
            dist += shelf.SpriteRendererParent.size.y;
        }

        _height = _shelves[0].SpriteRendererParent.size.y;
        
    }


    private void Update()
    {
        if(!_isActive) return;
        
        if (!_isClick)
        {
            float delta = Time.deltaTime * _speed;
            Move(delta);
        }
    }

    public void ShelveClickDown()
    {
        _isClick = true;
    }

    public void ShelveClickUp()
    {
        _isClick = false;
    }

    public void Move(float delta)
    {
        if (_deltaDist>0 && _deltaDist >= _height)
        {
            _deltaDist -= _height; 
            Shelve shelve = _shelves[0];
            shelve.TransformParent.position =
                (Vector2) _shelves[_shelves.Count - 1].TransformParent.position + Vector2.down * _height;
            _shelves.Remove(shelve);
            _shelves.Add(shelve);
        }

        if (_deltaDist < 0 && _deltaDist <= -_height)
        {
            _deltaDist += _height;
            Shelve shelve = _shelves[_shelves.Count-1];
            shelve.TransformParent.position =
                (Vector2) _shelves[0].TransformParent.position + Vector2.up * _height;
            _shelves.Remove(shelve);
            _shelves.Insert(0, shelve);
        }

        foreach (var shelf in _shelves)
        {
            shelf.TransformParent.Translate(Vector2.up * delta);
        }
        
        _deltaDist += delta;
    }

    public void CloseBox()
    {
        _isClick = true;
        BoxUp.transform.DOLocalMove(TargetClose.localPosition, 1f).SetEase(Ease.Linear);
    }

    public void OpenBox()
    {
        _isClick = false;
        BoxUp.transform.DOLocalMove(_startBoxUpPosition, 1f).SetEase(Ease.Linear);
        _isActive = true;
    }

    public void MoveToStart()
    {
        transform.DOMove(DownRight, 1f).SetEase(Ease.Linear).onComplete += OpenBox;
    }
    
    public void MoveToBack()
    {
        transform.DOMove(DownRight*10f, 1f).SetEase(Ease.Linear);
    }

    public void InitChildStage()
    {
        MoveToStart();
    }
}
