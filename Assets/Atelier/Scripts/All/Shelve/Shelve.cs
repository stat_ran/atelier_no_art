﻿using System;
using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;

public class Shelve : BaseBehavior
{
   [HideInInspector] public SpriteRenderer SpriteRendererParent;
   
   private SpriteRenderer _spriteRenderer;
   private ShelvesParent _shelvesParent;
   private bool _isClick;
   private bool _isMove;
   private Vector2 _startPointClick;
   [HideInInspector] public Transform TransformParent;
   private Vector2 _lastPoint;
   private Vector2 _startPoint;
   private Decor _decor;
   private BoxCollider2D _collider;
   private bool _isColor;
   private Color _color;
   private Sprite _colorSprite;
   
   public override void Awake()
   {
      base.Awake();
      
   }

   public void Init(Decor decor)
   {
      _isColor = false;
      _spriteRenderer = GetComponent<SpriteRenderer>();
      TransformParent = transform.parent;
      _shelvesParent = GetComponentInParent<ShelvesParent>();
      SpriteRendererParent = TransformParent.GetComponent<SpriteRenderer>();
      _startPoint = transform.localPosition;
      _decor = decor;
      _spriteRenderer.sprite = _decor.GetTap();
      _collider = GetComponent<BoxCollider2D>();
   }
   
   public void Init(Color color, Sprite sprite)
   {
      _isColor = true;
      _color = color;
      _colorSprite = sprite;
      _spriteRenderer = GetComponent<SpriteRenderer>();
      TransformParent = transform.parent;
      _shelvesParent = GetComponentInParent<ShelvesParent>();
      SpriteRendererParent = TransformParent.GetComponent<SpriteRenderer>();
      _startPoint = transform.localPosition;
      _spriteRenderer.sprite = _colorSprite;
      _spriteRenderer.color = color;
      _collider = GetComponent<BoxCollider2D>();
   }

   public override void Start()
   {
      base.Start();
   }

   private void OnMouseDown()
   {
      _isClick = true;
      _isMove = false;
      _startPointClick = MousePosition;
      _shelvesParent.ShelveClickDown();
      _lastPoint = MousePosition;
   }

   private void OnMouseDrag()
   {
      if (_isMove)
      {
         transform.position = MousePosition;
      }

      if (!_isMove && _isClick)
      {
         if (Mathf.Abs(MousePosition.x - _startPointClick.x) > 1f) _isMove = true;
      }

      if (!_isMove && _isClick)
      {
         _shelvesParent.Move((MousePosition-_lastPoint).y);
      }

      _lastPoint = MousePosition;

   }

   public void HideObject()
   {
      _collider.enabled = false;
      _spriteRenderer.enabled = false;
   }

   public void ShowObject()
   {
      _collider.enabled = true;
      _spriteRenderer.enabled = true;
   }

   private void OnMouseUp()
   {
      if (_isMove)
      {
         if (!_isColor)
         {
            if (Vector2.Distance(transform.position,
               _shelvesParent.Controller.BasePrefab.DecorsPoints[_shelvesParent.NumberDecor].SpriteRendererPositions[0]
                  .transform.position) < 5f)
            {
               _shelvesParent.Controller.BasePrefab.SetDecor(_decor);
               NotificationCenter.postNotification(Notifications.Get_Accessuary, null);
            }

            transform.localPosition = _startPoint;
         }
         else
         {
            if (Vector2.Distance(transform.position,
               _shelvesParent.Controller.CurrentBaseStage.ColorPoint.position) < 4f)
            {
               _shelvesParent.Controller.BasePrefab.SetColor(_color, _colorSprite);
               NotificationCenter.postNotification(Notifications.Get_Color, null);
            }
            transform.localPosition = _startPoint;
         }
      }

      _isClick = false;
      _isMove = false;
      _shelvesParent.ShelveClickUp();
   }

   private void ReachToTarget()
   {
      if(_shelvesParent.CurrentDecor!=null) Destroy(_shelvesParent.CurrentDecor);
      //TODO префаб получает декор!!!
      //Кинули партиклы
      NotificationCenter.postNotification(Notifications.Get_Accessuary, null);
   }

}
