﻿using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;
using UnityEngine.Serialization;

public class BaseStage : BaseBehavior
{

    [HideInInspector] public BaseController baseController;
    public Transform ColorPoint;

    public virtual void Init(BaseController baseController)
    {
        this.baseController = baseController;
        gameObject.SetActive(true);
        IChildStage[] childStages = GetComponentsInChildren<IChildStage>();
        foreach (var childStage in childStages)
        {
            childStage.InitChildStage();
        }
    }

    public virtual void Complete()
    {
        baseController.NextStage();
        gameObject.SetActive(false);
    }

}
