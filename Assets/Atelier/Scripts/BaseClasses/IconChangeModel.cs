﻿using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;

public class IconChangeModel : BaseBehavior
{
    [HideInInspector] public Stage_ChangeModel stageChangeModel;
    [HideInInspector] public BaseClothesPrefab ClothesPrefab;

    private SpriteRenderer _spriteRenderer;

    public override void Awake()
    {
        base.Awake();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    
    public void Init(Stage_ChangeModel stageChangeModel, BaseClothesPrefab clothesPrefab)
    {
        this.stageChangeModel = stageChangeModel;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        SpriteRenderer sr = GetComponentsInChildren<SpriteRenderer>()[1];
        sr.sprite = clothesPrefab.Icon;
        ClothesPrefab = clothesPrefab;
    }

    public virtual void OnClick()
    {
        stageChangeModel.baseController.SetPrefab(ClothesPrefab);
        stageChangeModel.Complete();
    }

    public float GetWidth()
    {
        if (_spriteRenderer == null) _spriteRenderer = GetComponent<SpriteRenderer>();
        return _spriteRenderer.size.x;
    }

}
