﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishStage : BaseStage
{
    public Transform ClothesPoint;
    public GameObject Projects; //Прожекторы
    public GameObject Prizel; //Объектив
    public GameObject PhotoFrame; //Фоторамка
    public Transform  PrefabPointToFrame;

    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        baseController.BasePrefab.transform.parent = ClothesPoint;
        baseController.BasePrefab.transform.position = ClothesPoint.position;
        //baseController.BasePrefab.transform.parent = ClothesPoint;
        Projects.SetActive(true);
        Prizel.SetActive(false);
        PhotoFrame.SetActive(false);
        StartCoroutine(TimerToPrizel());
        baseController.BasePrefab.Save();
    }

    IEnumerator TimerToPrizel()
    {
        yield return new WaitForSeconds(2f);
        Prizel.SetActive(true);
        yield return new WaitForSeconds(2f);
        baseController.BasePrefab.transform.parent = PrefabPointToFrame;
        baseController.BasePrefab.transform.localPosition=Vector3.zero;
        Prizel.SetActive(false);
        Projects.SetActive(false);
        PhotoFrame.SetActive(true);
        GC.NamePrefab_PhotoFrame = baseController.BasePrefab.name;
        GC.DepartmentType_Photo = baseController.BasePrefab.DepartmentType;
        StartCoroutine(LoadYourAsyncScene());
    }

    IEnumerator LoadYourAsyncScene()
    {
        GC.IsBuy = true;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Atelier/Scenes/MainMenu");
        
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
