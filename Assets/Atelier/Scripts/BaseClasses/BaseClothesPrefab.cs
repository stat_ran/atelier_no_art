﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using Atelier.Scripts;
using UnityEngine;
using UnityEngine.Accessibility;
using UnityEngine.Serialization;

public class BaseClothesPrefab : BaseBehavior
{

    public Sprite Icon;
    
    [HideInInspector] public DepartmentType DepartmentType;
    [HideInInspector] public int Number;
    [HideInInspector] public GameObject Maneken;
    [HideInInspector] public ColorForPrefab ColorForPrefab;
    [HideInInspector] public SpriteRenderer SpriteRenderer;
    
    public List<DecorForPrefab> DecorsPoints = new List<DecorForPrefab>();

    public override void Awake()
    {
        base.Awake();
        string[] ss = name.Split('_');
        Number = int.Parse(ss[1]);
        Transform[] allTransforms = GetComponentsInChildren<Transform>();
        foreach (var allTransform in allTransforms)
        {
            if (allTransform.name == "Maneken") Maneken = allTransform.gameObject;
        }

        SpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        gameObject.SetActive(false);
    }

    public void Activate(Transform pointPrefab)
    {
        gameObject.SetActive(true);
        transform.position = pointPrefab.position;
        transform.parent = pointPrefab;
    }

    public virtual void Save()
    {
        
    }

    public virtual void Load()
    {
        
    }

    public virtual void SetDecor(Decor decor)
    {
        Debug.Log(decor.Number);
        foreach (var decorsPoint in DecorsPoints)
        {
            if (decorsPoint.NumberDecor == decor.Number)
            {
                decorsPoint.Decor = decor;
                if (decor.Sprites.Length == 1)
                {
                    foreach (var spriteRenderer in decorsPoint.SpriteRendererPositions)
                    {
                        spriteRenderer.sprite = decor.Sprites[0];
                    }
                }

                if (decor.Sprites.Length > 1)
                {
                    for (int i = 0; i < decor.Sprites.Length; i++)
                    {
                        decorsPoint.SpriteRendererPositions[i].sprite = decor.Sprites[i];
                    }
                }
                //TODO Партиклы )
                return;
            }
        }
    }

    public virtual void SetColor(Color color, Sprite sprite)
    {
        ColorForPrefab.Color = color;
        SpriteRenderer.color = ColorForPrefab.Color;
        for (int i = 0; i < Data.Colors.Length; i++)
        {
            if (Data.Colors[i] == color)
            {
                ColorForPrefab.Number = i;
                return;
            }
        }
    }

    public void SavePrefab()
    {
        if (PlayerPrefs.HasKey(DepartmentType.ToString()))
        {
            DepartmentSavePrefs departmentSavePrefs =
                JsonUtility.FromJson<DepartmentSavePrefs>(PlayerPrefs.GetString(DepartmentType.ToString()));
            departmentSavePrefs.PrefabsName.Add(name);
            PlayerPrefs.SetString(DepartmentType.ToString(), JsonUtility.ToJson(departmentSavePrefs));
        }
        else
        {
            DepartmentSavePrefs departmentSavePrefs = new DepartmentSavePrefs();
            departmentSavePrefs.PrefabsName.Add(name);
            PlayerPrefs.SetString(DepartmentType.ToString(), JsonUtility.ToJson(departmentSavePrefs));
        }
        
        PlayerPrefs.Save();
    }

    public virtual void SetColor(int number)
    {
        ColorForPrefab.Number = number;
        ColorForPrefab.Color = Data.Colors[ColorForPrefab.Number];
        SpriteRenderer.color = ColorForPrefab.Color;
    }

}

[System.Serializable]
public class ColorForPrefab
{
    public int Number;
    public Color Color;
}

[System.Serializable]
public class DecorForPrefab
{
    public int NumberDecor;
    public SpriteRenderer[] SpriteRendererPositions;
    [HideInInspector] public Decor Decor;
}

[System.Serializable]
public class DepartmentSavePrefs
{
    public List<string> PrefabsName=new List<string>();
}
