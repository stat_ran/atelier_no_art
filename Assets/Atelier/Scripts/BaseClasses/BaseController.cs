﻿using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BaseController : BaseBehavior
{

   
   public List<BaseStage> Stages;
   
   [HideInInspector] public BaseClothesPrefab BasePrefab;
   [HideInInspector] public DepartmentType Type;
   
   private BaseStage _finishStage;
   private int _numberStage;
   [HideInInspector] public BaseStage CurrentBaseStage;
   

   public override void Start()
   {
      base.Start();
      Stages[0].gameObject.SetActive(true);
      for (int i = 1; i < Stages.Count; i++)
      {
         Stages[i].gameObject.SetActive(false);
      }

      _finishStage = Instantiate(GC.dataContainer.FinishStagePrefab, Vector3.zero, Quaternion.identity).GetComponent<BaseStage>();
      _finishStage.gameObject.SetActive(false);
      NextStage();
   }
   
   public virtual void Update()
   {
      if (Input.GetKeyDown(KeyCode.L))
      {
         NextStage();
      }
   }
   
   public virtual void Complete()
   {
      CurrentBaseStage.gameObject.SetActive(false);
      _finishStage.Init(this);
   }
   
   public void NextStage()
   {
        
      if (CurrentBaseStage != null)
      {
         GC.GameCamera.MatrixEffect._Play();
      }
        
      if (_numberStage >= Stages.Count)
      {
         Complete();
      }
      else
      {
         CurrentBaseStage = Stages[_numberStage];
         CurrentBaseStage.Init(this);
      }
      _numberStage++;
   }

   public virtual void SetPrefab(BaseClothesPrefab clothesPrefab)
   {
      BasePrefab = Instantiate(clothesPrefab.gameObject).GetComponent<BaseClothesPrefab>();
      BasePrefab.name = clothesPrefab.name;
   }

   public void Back()
   {
      SceneManager.LoadScene("MainMenu");
   }

}
