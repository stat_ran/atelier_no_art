﻿using System.Collections.Generic;
using AceCream.Scripts;
using Atelier.Scripts;
using UnityEngine;

//Довести до ума модель, чтоб можно было без проблем брать контент
//ПРефаб сцены с полками
//Фотка
//Контейнер для фоток, получаем на старте
//Передача изделия от сцены к сцене - Нужно в базовый класс поместить


namespace Atelier.Scripts
{
    public class BaseBehavior : MonoBehaviour
    {
        public static GameController GC;
        public static GameCamera Camera;

        private Transform _cachedTransform;
        private bool _transformIsCached;
        private Vector2 _middleLeft;
        private Vector2 _middleRight;
        private DataContainer _data;

        public new Transform transform
        {
            get
            {
                if (_transformIsCached) return _cachedTransform;
                _cachedTransform = base.transform;
                _transformIsCached = _cachedTransform != null;
                return _cachedTransform;
            }
        }

        public DataContainer Data
        {
            get
            {
                if (_data==null)
                {
                    _data = GC.dataContainer;
                }
                return _data;
            }
        }

        public Vector2 MiddleLeft
        {
            get
            {
                _middleLeft.x = (Camera.GetScreenCornersWorldPosition().upperLeft.x +
                                 Camera.GetScreenCornersWorldPosition().lowerLeft.x) / 2f;
                _middleLeft.y = (Camera.GetScreenCornersWorldPosition().upperLeft.y +
                                 Camera.GetScreenCornersWorldPosition().lowerLeft.y) / 2f;
                return _middleLeft;
            }
        }


        public Vector2 MiddleRight
        {
            get
            {
                _middleRight.x = (Camera.GetScreenCornersWorldPosition().upperRight.x +
                                 Camera.GetScreenCornersWorldPosition().lowerRight.x) / 2f;
                _middleRight.y = (Camera.GetScreenCornersWorldPosition().upperRight.y +
                                 Camera.GetScreenCornersWorldPosition().lowerRight.y) / 2f;
                return _middleRight;
            }
        }

        public Vector2 DownLeft => Camera.GetScreenCornersWorldPosition().lowerLeft;
        public Vector2 DownRight => Camera.GetScreenCornersWorldPosition().lowerRight;
        public Vector2 UpLeft => Camera.GetScreenCornersWorldPosition().upperLeft;
        public Vector2 UpRight => Camera.GetScreenCornersWorldPosition().upperRight;

        public Vector2 MousePosition
        {
            get
            {
#if UNITY_EDITOR
                return Camera.Camera.ScreenToWorldPoint(Input.mousePosition);
#else
                return Camera.Camera.ScreenToWorldPoint(Input.GetTouch(0).position);
#endif
            }
        }

        public virtual void Awake()
        {
            if(GameController.Instance==null) FindObjectOfType<GameController>().Init();
            GC = GameController.Instance;
            Camera = GC.GameCamera;
        }

        public virtual void Start()
        {
        }

        public virtual void OnOverlapStart()
        {
        }

        public virtual void OnOverlapEnd()
        {
        }

        public virtual void OnMouseMove(Vector2 worldPoint)
        {
        }

        public virtual void OnMouseButtonDown2D(List<BaseBehavior> allSelectedItems)
        {
        }

        public virtual void OnMouseButtonUp2D(List<BaseBehavior> allSelectedItems)
        {
        }
    }
}