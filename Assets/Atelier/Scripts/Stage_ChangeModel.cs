﻿using System;
using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;
using UnityEngine.Serialization;

public class Stage_ChangeModel : BaseStage
{

    public DepartmentType departmentType;
    [HideInInspector] public List<IconChangeModel> ButtonChangeModels=new List<IconChangeModel>();
    
    private bool _isMove;
    private bool _isSlider;
    private float _speed = 1f;
    private float _speedSwipe = 100f;
    private bool _directLeft;
    private Vector2 _lastPositionMouse;
    private Vector2 _deltaMove;
    private Vector2 _startTapPosition;
    private Vector2 _currentPosition;

    public override void Awake()
    {
        base.Awake();
        ButtonChangeModels.Clear();
        foreach (var prefab in Data.Departments[departmentType].Prefabs)
        {
            IconChangeModel obj = Instantiate(Data.IconChangeModel.gameObject, transform).GetComponent<IconChangeModel>();
            ButtonChangeModels.Add(obj);
            obj.Init(this, prefab);
        }
        GC.IsBuy = false;
    }

    public override void Start()
    {
        base.Start();

        ButtonChangeModels[0].transform.position =
            MiddleLeft + ButtonChangeModels[0].GetWidth() / 2f * Vector2.right;
        for (int i = 1; i < ButtonChangeModels.Count; i++)
        {
            ButtonChangeModels[i].transform.position =
                (Vector2) ButtonChangeModels[i - 1].transform.position +
                Vector2.right * (ButtonChangeModels[i-1].GetWidth()/2f + ButtonChangeModels[i].GetWidth()/2f + 1);
        }

        _isMove = true;
    }

    private void Update()
    {

#if !UNITY_EDITOR
         if (Input.touchCount != 1) return;
         _currentPosition = Camera.Camera.ScreenToWorldPoint(Input.GetTouch(0).position);
#endif
#if UNITY_EDITOR
        _currentPosition=Camera.Camera.ScreenToWorldPoint(Input.mousePosition);
#endif
        
        
        if (!_isMove && Input.GetMouseButton(0))
        {
            if (!_isSlider && (_currentPosition - _startTapPosition).magnitude > 0.5f) _isSlider = true;

            if (_isSlider)
            {
                _deltaMove = _currentPosition - _lastPositionMouse;
                _directLeft = !(_currentPosition.x > _lastPositionMouse.x);
                foreach (var model in ButtonChangeModels)
                {
                    model.transform.Translate( _deltaMove.x * _speedSwipe * Time.deltaTime * Vector2.right);
                }
            }
            _lastPositionMouse = _currentPosition;
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            _isMove = false;
            _startTapPosition = _currentPosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (!_isSlider)
            {
                RaycastHit2D[] hit2D = Physics2D.RaycastAll(_currentPosition, Vector2.zero);
                foreach (var raycastHit2D in hit2D)
                {
                    IconChangeModel iButton = raycastHit2D.collider.GetComponent<IconChangeModel>();
                    if(iButton!=null) raycastHit2D.collider.GetComponent<IconChangeModel>().OnClick();
                }
            }
            _isMove = true;
            _isSlider = false;
        }

        if (_isMove)
        {
            foreach (var changeModel in ButtonChangeModels)
            {
                changeModel.transform.Translate(_speed * Time.deltaTime * Vector2.left);
            }
        }
        
        Swap(_directLeft);

    }

    private void Swap(bool direct)
    {

        if (
            ButtonChangeModels[ButtonChangeModels.Count - 1].transform.position.x +
            ButtonChangeModels[ButtonChangeModels.Count - 1].GetWidth() / 2f <
            MiddleRight.x)
        {          
            IconChangeModel iButtonIconChangeModel = ButtonChangeModels[0];
            iButtonIconChangeModel.transform.position =
                (Vector2) ButtonChangeModels[ButtonChangeModels.Count - 1].transform.position +
                (ButtonChangeModels[0].GetWidth() / 2f +
                 ButtonChangeModels[ButtonChangeModels.Count - 1].GetWidth() / 2f + 1) * Vector2.right;
            ButtonChangeModels.Remove(iButtonIconChangeModel);
            ButtonChangeModels.Add(iButtonIconChangeModel);
        }

        if (
            ButtonChangeModels[0].transform.position.x - ButtonChangeModels[0].GetWidth() / 2f >
            MiddleLeft.x)
        {           
            IconChangeModel iButtonIconChangeModel = ButtonChangeModels[ButtonChangeModels.Count - 1];
            iButtonIconChangeModel.transform.position =
                (Vector2) ButtonChangeModels[0].transform.position -
                (ButtonChangeModels[ButtonChangeModels.Count - 1].GetWidth() / 2f +
                 ButtonChangeModels[0].GetWidth() / 2f + 1) * Vector2.right;
            ButtonChangeModels.Remove(iButtonIconChangeModel);
            ButtonChangeModels.Insert(0, iButtonIconChangeModel);
        }
    }

}
