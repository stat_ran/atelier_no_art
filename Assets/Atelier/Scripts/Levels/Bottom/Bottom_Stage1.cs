using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bottom_Stage1 : BaseStage
{

    public List<GameObject> Cutters;

    public override void Start()
    {
        base.Start();
        
    }

    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        foreach (var cutter in Cutters)
        {
            cutter.SetActive(false);
        }
        string[] str = base.baseController.BasePrefab.name.Split('_');
        Cutters[int.Parse(str[1])-1].SetActive(true);
    }

    public override void Complete()
    {
        base.Complete();
        
    }
}
