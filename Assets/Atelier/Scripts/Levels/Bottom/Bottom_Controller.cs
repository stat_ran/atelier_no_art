using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bottom_Controller : BaseController
{
    [HideInInspector] public BottomPrefab Prefab;

    public override void Awake()
    {
        base.Awake();
        Type = DepartmentType.Bottom;
    }

    public override void SetPrefab(BaseClothesPrefab clothesPrefab)
    {
        base.SetPrefab(clothesPrefab);
        Prefab=BasePrefab as BottomPrefab;
    }
}
