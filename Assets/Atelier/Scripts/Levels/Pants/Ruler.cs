﻿using DG.Tweening;
using UnityEngine;

public class Ruler : MonoBehaviour
{
    public Transform Paper;
    public Transform RulerPoint;
    public SpriteRenderer SpriteRuler;
    public Transform Pencil;

    private Transform[] _nodes;
    private int _nextNode = 1;
    private float _timeSpeed = 0.5f;
    private bool _isDrawing;
    private Paint _paint;
    private Vector2 _prevPosition;
    private bool _isMove;
    
    [HideInInspector] public Vector2 FirstPosition;
    [HideInInspector] public Vector2 SwipeDirection;
    [HideInInspector] public bool IsDown;
    [HideInInspector] public bool IsMovementAllowed = true;
    [HideInInspector] public Vector2 PrevPos;
    [HideInInspector] public Vector2 FirstGlobalPosition;
    private bool _isFirstTouch = false;
    [HideInInspector] public Transform Transform;
    private Camera _camera;

    public void Start()
    {
        Transform = transform;
        _camera=Camera.main;

        _nodes = new Transform[Paper.childCount-1];
        for (int i = 0; i < Paper.childCount-1; i++)
            _nodes[i] = Paper.GetChild(i);

        IsMovementAllowed = false;

        Paper.position += RulerPoint.position - _nodes[0].position;
        Vector2 nodeDirection = _nodes[1].position - _nodes[0].position;

        _paint = GetComponent<Paint>();
    }

    private void LateUpdate()
    {
        SpriteRuler.size = new Vector2(2.5f + (Transform.position.x + 1.5f) * 23/18, SpriteRuler.size.y);
        if (_isDrawing)
        {
            if (Pencil.position.x > RulerPoint.position.x && Pencil.position.x <= _nodes[_nextNode % _nodes.Length].position.x + Pencil.position.x - _prevPosition.x)
                _paint.DrawLine(_prevPosition.x > RulerPoint.position.x ? _prevPosition : (Vector2)RulerPoint.position, 
                    Pencil.position.x < _nodes[_nextNode % _nodes.Length].position.x ? Pencil.position : _nodes[_nextNode % _nodes.Length].position);
            _prevPosition = Pencil.position;
        }
    }

    private void PaintEnd()
    {
        _isDrawing = false;
        Transform.DOMove(FirstGlobalPosition, _timeSpeed * Vector2.Distance(Transform.position, FirstGlobalPosition) / 10).SetEase(AnimationCurve.Linear(0, 0, 1, 1)).onComplete = ComputeMove;
    }

    private void ComputeMove()
    {
        if (_nextNode >= _nodes.Length)
        {
            LastMove();
            return;
        }

        Vector2 nodeDirection = _nodes[(_nextNode + 1) % _nodes.Length].position - _nodes[_nextNode].position;
        Vector3 euler = Quaternion.FromToRotation(RulerPoint.right, nodeDirection).eulerAngles;

        Paper.Rotate(-euler);
        Paper.DOMove(Paper.position + RulerPoint.position - _nodes[_nextNode].position, _timeSpeed).SetEase(AnimationCurve.Linear(0, 0, 1, 1));
        Paper.Rotate(euler);

        Paper.DOLocalRotate(Paper.localEulerAngles - euler, _timeSpeed).SetEase(AnimationCurve.Linear(0, 0, 1, 1)).onComplete+=EndDrawing;

        _nextNode++;
    }

    private void EndDrawing()
    {
        _isMove = false;
    }

    private void LastMove()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        SpriteRuler.GetComponent<SpriteRenderer>().enabled = false;

        Vector3 euler = Quaternion.FromToRotation(RulerPoint.right, Paper.right).eulerAngles;

        Paper.Rotate(-euler);
        Paper.DOMove(Vector3.zero, _timeSpeed).SetEase(AnimationCurve.Linear(0, 0, 1, 1));
        Paper.DOScale(Vector3.one * 0.55f, _timeSpeed).SetEase(AnimationCurve.Linear(0, 0, 1, 1));
        Paper.Rotate(euler);

        Paper.DOLocalRotate(Paper.localEulerAngles - euler, _timeSpeed).SetEase(AnimationCurve.Linear(0, 0, 1, 1)).onComplete = Finish;
    }

    private void Finish()
    {
        FindObjectOfType<Bottom_Stage1>(true).Complete();
    }
    
    private void OnMouseDown()
    {
        if(_isMove) return;
#if !UNITY_EDITOR
         if (Input.touchCount != 1) return;
         PrevPos = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);
#endif
#if UNITY_EDITOR
        PrevPos = _camera.ScreenToWorldPoint(Input.mousePosition);
#endif
       
        FirstPosition = Transform.localPosition;
        FirstGlobalPosition = Transform.position;
        IsDown = true;
        _isFirstTouch = true;
        SwipeDirection = Vector2.zero;
    }

    private void OnMouseDrag()
    {
        if (_isFirstTouch)
        {
            if(_isMove) return;
#if !UNITY_EDITOR
            Vector2 touchPos = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);
            SwipeDirection = Input.GetTouch(0).deltaPosition;
#endif
#if UNITY_EDITOR
            Vector2 touchPos = _camera.ScreenToWorldPoint(Input.mousePosition);
            SwipeDirection = touchPos - PrevPos;
#endif
            
            if (IsMovementAllowed)
                Transform.position = new Vector3(touchPos.x, touchPos.y, Transform.position.z);

            PrevPos = touchPos;
            

            Transform.position = new Vector2(Mathf.Clamp(Transform.position.x + SwipeDirection.x, Transform.position.x, 7.5f), Transform.position.y);//Hard code!
        }
    }

    private void OnMouseUp()
    {
        if (_isFirstTouch)
        {
            if(_isMove) return;
            IsDown = false;
            _isFirstTouch = false;
            SwipeDirection = Vector2.zero;
            PrevPos = Vector2.zero;
            

            Vector2 nodeDirection = _nodes[_nextNode % _nodes.Length].position - _nodes[_nextNode - 1].position;
            float dist = Vector2.Distance(Transform.position, FirstGlobalPosition);

            if (dist > nodeDirection.magnitude / 2)
            {
                _isDrawing = true;
                _isMove = true;
                _prevPosition = Pencil.position;
                Pencil.position = new Vector2(-15f, RulerPoint.position.y);
                Pencil.DOMove(new Vector2(15f, RulerPoint.position.y), _timeSpeed * 2f).SetEase(AnimationCurve.Linear(0, 0, 1, 1)).onComplete = PaintEnd;
            }
            else
            {
                Transform.DOMove(FirstGlobalPosition, _timeSpeed * dist / 10).SetEase(AnimationCurve.Linear(0, 0, 1, 1));
            }
        }
    }
}
