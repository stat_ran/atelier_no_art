﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Scarf_Stage1 : BaseStage
{

    public Transform ThreadPoint;
    public Transform Machine;
    public SpriteRenderer Thread;
    public GameObject StitchPrefab;
    public Transform StitchesParent;
    public GameObject KnotPrefab;
    public SpriteRenderer Ball;
    public int NumberColor;

    private List<GameObject> _stitches= new List<GameObject>();
    private float _deltaMove = 0.8f;
    private float _speed = 0.2f;
    private int orderLayerStitch;
    private Vector2 _spawnPointStitch;
    private Vector2 _endPointKnot;
    private int _countStitch;
    private Scarf_Controller _controller;
    private ScarfPrefab _scarfPrefab;

    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        _controller = base.baseController.GetComponent<Scarf_Controller>();
        InitTread();
        orderLayerStitch = Machine.GetComponent<SpriteRenderer>().sortingOrder - 1;
        _spawnPointStitch = StitchesParent.position;
        _endPointKnot=new Vector2(_spawnPointStitch.x, ThreadPoint.position.y);
        StartCoroutine(SpawnKnot());
        Ball.color = _controller.Prefab.Colors[NumberColor];
        Thread.color = _controller.Prefab.Colors[NumberColor];
    }

    IEnumerator SpawnKnot()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            GameObject obj = Instantiate(KnotPrefab, ThreadPoint.position, Quaternion.identity, transform);
            obj.GetComponent<Scarf_Stage1_Knot>().StartMove(_endPointKnot, 4f);
            obj.GetComponent<SpriteRenderer>().color = _controller.Prefab.Colors[NumberColor];
        }
    }

    private void InitTread() //Ставим нить на место и с нужным размером
    {
        Thread.transform.position = ThreadPoint.position;
        Thread.size = new Vector2(Mathf.Abs(Machine.position.x) + Mathf.Abs(ThreadPoint.position.x), Thread.size.y);
        Thread.color = _controller.Prefab.Colors[NumberColor];
    }

    public void InstantiateStitch()
    {
        _countStitch++;
        GameObject obj = Instantiate(StitchPrefab, _spawnPointStitch, Quaternion.identity, StitchesParent);
        _stitches.Add(obj);
        SpriteRenderer sr = obj.GetComponent<SpriteRenderer>();
        sr.sortingOrder = orderLayerStitch;
        sr.color = _controller.Prefab.Colors[NumberColor];
        StitchesParent.DOMove((Vector2) StitchesParent.position + Vector2.right * _deltaMove, _speed)
            .SetEase(Ease.Linear).onComplete += StitchComplete;
    }

    private void StitchComplete()
    {
        if(_countStitch>=10) Complete();
    }
    
}
