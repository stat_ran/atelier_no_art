﻿using System;
using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;

public class Scarf_Controller : BaseController
{
    [HideInInspector] public ScarfPrefab Prefab;

    public override void Awake()
    {
        base.Awake();
        Type = DepartmentType.Scarf;
    }

    public override void SetPrefab(BaseClothesPrefab clothesPrefab)
    {
        base.SetPrefab(clothesPrefab);
        Prefab=BasePrefab as ScarfPrefab;
    }
}
