﻿using System;
using System.Collections;
using System.Collections.Generic;
using AceCream.Scripts;
using Atelier.Scripts;
using DG.Tweening;
using UnityEngine;

public class Scarf_Stage1_Knot : BaseBehavior
{
    public void StartMove(Vector2 point, float speed)
    {
        transform.DOMove(point, speed).SetEase(Ease.Linear).onComplete += EndMove;
    }

    private void EndMove()
    {
        Destroy(gameObject, 0.04f);
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        transform.DOKill();
    }
}
