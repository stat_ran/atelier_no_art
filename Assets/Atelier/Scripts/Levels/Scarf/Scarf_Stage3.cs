﻿using System;
using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;
using UnityEngine.Serialization;

public class Scarf_Stage3 : BaseStage
{

    private Scarf_Controller _controller;
    public Transform NextStageButton;
    public Transform PrefabPoint;

    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        _controller = baseController as Scarf_Controller;
        _controller.Prefab.transform.position = PrefabPoint.position;
        _controller.Prefab.Activate(PrefabPoint);
        
        if (NextStageButton != null)
            NextStageButton.gameObject.SetActive(false);
    }

    public override void Start()
    {
        base.Start();
        NotificationCenter.addCallback(Notifications.Get_Accessuary, DecorSetup);
    }

    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.Get_Accessuary, DecorSetup);
    }

    private void DecorSetup(object obj)
    {
        //Повесили декор
        //Можно кнопку перехода показыать
        if (NextStageButton != null)
            NextStageButton.gameObject.SetActive(true);
    }

    public void NextButton()
    {
        Complete();
    }

}
