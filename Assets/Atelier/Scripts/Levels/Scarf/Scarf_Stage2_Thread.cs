﻿using UnityEngine;

public class Scarf_Stage2_Thread : MonoBehaviour
{

    private SpriteRenderer _spriteRenderer;
    private Transform _transform;

    private Vector2 _firstPoint;
    private Vector2 _secondPoint;

    public void Init(Vector2 firstPoint, Vector2 secondPoint, Color color, int sortingOrder)
    {
        _firstPoint = firstPoint;
        _secondPoint = secondPoint;
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        _transform = transform;
        _transform.localPosition = firstPoint;
        Vector2 dir = secondPoint - firstPoint;
        _spriteRenderer.size = new Vector2(Vector2.Distance(firstPoint, secondPoint) + 0.2f, _spriteRenderer.size.y);
        _transform.rotation=Quaternion.FromToRotation(Vector2.right, dir);
        _spriteRenderer.color = color;
        _spriteRenderer.sortingOrder = sortingOrder;
    }

    public void MiddleLine(Vector2 delta)
    {
        _secondPoint += delta;
        Vector2 dir = _secondPoint - _firstPoint;
        _spriteRenderer.size = new Vector2(Vector2.Distance(_firstPoint, _secondPoint)+0.2f,
            _spriteRenderer.size.y);
        _transform.rotation = Quaternion.FromToRotation(Vector2.right, dir);
    }

    public void Tighten(float delta)
    {
        _secondPoint.y = _secondPoint.y < 0 ? _secondPoint.y + delta*2f: _secondPoint.y;
        _firstPoint.y = _firstPoint.y < 0 ? _firstPoint.y + delta*2f: _firstPoint.y;
        _transform.localPosition = _firstPoint;
        Vector2 dir = _secondPoint - _firstPoint;
        _spriteRenderer.size = new Vector2(Vector2.Distance(_firstPoint, _secondPoint)+0.2f,
            _spriteRenderer.size.y);
        _transform.rotation = Quaternion.FromToRotation(Vector2.right, dir);
    }

}
