using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Atelier.Scripts;
using UnityEditor;
using UnityEngine;

public class Hat_Stage3_Rolik : BaseBehavior
{
    public SpriteRenderer SpriteRendererRolik;
    public Transform StartPoint;
    public Transform EndPoint;

    private Hat_Controller _controller;
    private SpriteRenderer _targetSpriteRenderer;
    private bool _isMove;
    private Vector2 _lastPosition;
    private float _whith;
    private float _currentPositionPaint;
    private Sprite _sprite;
    private Sprite _currentSprite;

    private int _count;

    public void Init()
    {
        transform.position = StartPoint.position;
        _controller = FindObjectOfType<Hat_Controller>();
        _targetSpriteRenderer = _controller.Prefab.DecorUzor;
        _targetSpriteRenderer.enabled = true;
        _currentSprite = _targetSpriteRenderer.sprite;
        SpriteRendererRolik.sprite = _currentSprite;
        
        Texture2D targetTexture = new Texture2D(_targetSpriteRenderer.sprite.texture.width,
            _targetSpriteRenderer.sprite.texture.height);
        targetTexture.SetPixels(_targetSpriteRenderer.sprite.texture.GetPixels());
        targetTexture.Apply();
        Debug.Log(_currentSprite.pivot);
        var pivot = new Vector2(_currentSprite.pivot.x / _currentSprite.texture.width,
            _currentSprite.pivot.y / _currentSprite.texture.height);
        _sprite = Sprite.Create(targetTexture, new Rect(0, 0, targetTexture.width, targetTexture.height),
            pivot, _targetSpriteRenderer.sprite.pixelsPerUnit);
        _targetSpriteRenderer.sprite = _sprite;
        _whith = StartPoint.position.x - EndPoint.position.x;
        
        Color color;
        for (int i = 0; i < _sprite.texture.width; i++)
        {
            for (int j = 0; j < _sprite.texture.height; j++)
            {
                color = _sprite.texture.GetPixel(i, j);
                color.a = 0;
                _sprite.texture.SetPixel( i, j, color);
            }
        }
        _sprite.texture.Apply();
        _currentPositionPaint = 1f;
    }

    private void OnMouseDown()
    {
        _isMove = true;
        _lastPosition = MousePosition;
    }

    private void OnMouseDrag()
    {
        var delta = MousePosition.x - _lastPosition.x;
        transform.Translate(Vector2.right * delta);
        if (transform.position.x > StartPoint.position.x)
        {
            transform.position = StartPoint.position;
            delta = 0;
            if (_count == 1) _count = 2;
        }
        
        if (transform.position.x < EndPoint.position.x)
        {
            transform.position = EndPoint.position;
            delta = 0;
            if (_count == 0) _count = 1;
            if(_count==2) _controller.CurrentBaseStage.Complete();
        }

        SpriteRendererRolik.material.mainTextureOffset += Vector2.left * delta/5f;
        _lastPosition = MousePosition;
        if (_count == 0) PaintLeft((transform.position.x - EndPoint.position.x) / _whith);
        if(_count==1) PaintRight((transform.position.x - EndPoint.position.x) / _whith);
    }

    private void OnMouseUp()
    {
        _isMove = false;
    }

    private void PaintLeft(float position)
    {
        if (_currentPositionPaint < position) return;

        Sprite sprite = _targetSpriteRenderer.sprite;
        int start = (int) (sprite.texture.width * position);
        int end = (int) (sprite.texture.width * _currentPositionPaint) + 1;
        Color color;
        for (int i = start; i < end; i ++)
        {
            for (int j = 0; j < sprite.texture.height; j++)
            {
                color = sprite.texture.GetPixel(i, j);
                color.a = _currentSprite.texture.GetPixel(i,j).a/2f;
                sprite.texture.SetPixel(i, j, color);
            }
        }

        sprite.texture.Apply();
        _currentPositionPaint = position;
    }
    
    private void PaintRight(float position)
    {
        if (_currentPositionPaint > position) return;

        Sprite sprite = _targetSpriteRenderer.sprite;
        int start = (int) (sprite.texture.width * position)+1;
        int end = (int) (sprite.texture.width * _currentPositionPaint);
        Color color;
        for (int i = end; i < start; i ++)
        {
            for (int j = 0; j < sprite.texture.height; j++)
            {
                color = sprite.texture.GetPixel(i, j);
                color.a = _currentSprite.texture.GetPixel(i,j).a;
                sprite.texture.SetPixel(i, j, color);
            }
        }

        sprite.texture.Apply();
        _currentPositionPaint = position;
    }


}
