﻿using System.Collections;
using Atelier.Scripts;
using UnityEngine;
using UnityEngine.U2D;

public class Hat_Stage2_Igla : BaseBehavior
{
    public SpriteShapeController ShapeController;
    public Transform Igla;
    public SpriteRenderer ThreadPiece;
    private Vector2 _startPos1;
    private Vector2 _startPos2;

    private Hat_Stage2 _stage2;
    private Vector2 _lastPositionMouse;
    private Vector2 _delta;
    private bool _isClick;
    private float _lenght = 4f;
    private float _kX;
    private float _kY;
    private bool _isComplete;
    private bool _isActive;
    private float _speed = 20f;
    private Vector2 _startPositionIgla;
    
    public override void Start()
    {
        base.Start();
        _stage2 = FindObjectOfType<Hat_Stage2>();
        _isActive = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            ShapeController.spline.SetPosition(0, Vector2.left);
        }

        if(!_isActive) return;
        if (Input.GetMouseButtonUp(0))
        {
            _isClick = false;
            if (!_isComplete)
            {
                StartCoroutine(_stage2.IsUp ? MoveToDown() : MoveToUp());
            }
        }

        if (Input.GetMouseButton(0) && _isClick)
        {
            Vector2 pos = Camera.Camera.ScreenToWorldPoint(Input.mousePosition);
            _delta = pos - _lastPositionMouse;
            _lastPositionMouse = Camera.Camera.ScreenToWorldPoint(Input.mousePosition);
            if(_stage2.IsUp && _startPositionIgla.y<Igla.position.y+_delta.y) return;
            if(!_stage2.IsUp && _startPositionIgla.y>Igla.position.y+_delta.y) return;
            if (_stage2.IsUp)
            {
                ShapeController.spline.SetPosition(2, new Vector2(ShapeController.spline.GetPosition(2).x,
                    ShapeController.spline.GetPosition(2).y + _delta.y));
                ShapeController.spline.SetPosition(1, new Vector2(ShapeController.spline.GetPosition(1).x + _delta.y * _kX*0.72f,
                    ShapeController.spline.GetPosition(1).y + _delta.y * _kY/1.6f));
                Igla.position = ShapeController.spline.GetPosition(2) + transform.position;
            }
            else
            {
                ShapeController.spline.SetPosition(2, new Vector2(ShapeController.spline.GetPosition(2).x,
                    ShapeController.spline.GetPosition(2).y - _delta.y));
                ShapeController.spline.SetPosition(1, new Vector2(ShapeController.spline.GetPosition(1).x - _delta.y * _kX*0.72f,
                    ShapeController.spline.GetPosition(1).y - _delta.y * _kY/1.6f));
                Igla.position = transform.position - ShapeController.spline.GetPosition(2);
            }

            

            if (_stage2.IsUp && _stage2.StartPointThread.y - Igla.position.y > _lenght)
            {
                Complete();
            }
            else if(!_stage2.IsUp && Igla.position.y - _stage2.StartPointThread.y+_lenght > _lenght*1.5f)
            {
                Complete();
            }

        }

        if (Input.GetMouseButtonDown(0))
        {
            _isClick = true;
            _lastPositionMouse = Camera.Camera.ScreenToWorldPoint(Input.mousePosition);
        }
        
    }

    IEnumerator MoveToDown()
    {
        float speed = -_speed;
        while (true)
        {
            yield return null;
            ShapeController.spline.SetPosition(2, new Vector2(ShapeController.spline.GetPosition(2).x,
                ShapeController.spline.GetPosition(2).y + speed*Time.deltaTime));
            ShapeController.spline.SetPosition(1, new Vector2(ShapeController.spline.GetPosition(1).x + speed*Time.deltaTime * _kX*0.72f,
                ShapeController.spline.GetPosition(1).y + speed*Time.deltaTime * _kY/1.6f));
            Igla.position = ShapeController.spline.GetPosition(2) + transform.position;
            if (_stage2.StartPointThread.y - Igla.position.y > _lenght)
            {
                Complete();
                yield break;
            }
        }
    }
    
    IEnumerator MoveToUp()
    {
        float speed = _speed;
        while (true)
        {
            yield return null;
            ShapeController.spline.SetPosition(2, new Vector2(ShapeController.spline.GetPosition(2).x,
                ShapeController.spline.GetPosition(2).y - speed*Time.deltaTime));
            ShapeController.spline.SetPosition(1, new Vector2(ShapeController.spline.GetPosition(1).x - speed*Time.deltaTime * _kX*0.72f,
                ShapeController.spline.GetPosition(1).y - speed*Time.deltaTime * _kY/1.6f));
            Igla.position = transform.position - ShapeController.spline.GetPosition(2);
            if (Igla.position.y - _stage2.StartPointThread.y+_lenght > _lenght)
            {
                Complete();
                yield break;
            }
        }
    }

    public void InitThreadUp()
    {
        ShapeController.spline.SetPosition(2, new Vector2(0, 3.5f));
        ShapeController.spline.SetPosition(1, new Vector2(0, 3.4f));
        _kX = ((ShapeController.spline.GetPosition(2).x - ShapeController.spline.GetPosition(0).x) / 2f)/(_lenght);
        _kY = (ShapeController.spline.GetPosition(1).y - 1f) / _lenght;
        Igla.position= ShapeController.spline.GetPosition(2)+transform.position;
        _startPositionIgla = Igla.position;
        Igla.eulerAngles = Vector3.forward * 180f;
        Igla.GetComponent<SpriteRenderer>().sortingOrder = 4;
        ThreadPiece.sortingOrder = 5;
        transform.parent.eulerAngles=Vector3.zero;
        _isActive = true;
        _isComplete = false;
        StartCoroutine(UpdateIglaUp());
    }
    
    public void InitThreadDown()
    {
        ShapeController.spline.SetPosition(2, new Vector2(0, 1.5f));
        ShapeController.spline.SetPosition(1, new Vector2(-0.5f, 2f));
        _kX = ((ShapeController.spline.GetPosition(2).x - ShapeController.spline.GetPosition(0).x) / 2f)/_lenght;
        _kY = (ShapeController.spline.GetPosition(1).y - 1f) / _lenght;
        Igla.position = transform.position - ShapeController.spline.GetPosition(2);
        Igla.eulerAngles = Vector3.zero;
        Igla.GetComponent<SpriteRenderer>().sortingOrder = 7;
        ThreadPiece.sortingOrder = 8;
        transform.parent.eulerAngles = Vector3.forward * 180f;
        _isActive = true;
        _isComplete = false;
        if(gameObject.activeSelf) StartCoroutine(UpdateIglaDown());
    }

    IEnumerator UpdateIglaUp()
    {
        yield return new WaitForEndOfFrame();
        Igla.position= ShapeController.spline.GetPosition(2)+transform.position;
        _startPositionIgla = Igla.position;
    }
    IEnumerator UpdateIglaDown()
    {
        yield return new WaitForEndOfFrame();
        Igla.position= transform.position - ShapeController.spline.GetPosition(2);
        _startPositionIgla = Igla.position;
    }

    private void SetFaceToFace()
    {
        Vector2 dir1 = ShapeController.spline.GetPosition(2)-ShapeController.spline.GetPosition(1);
        ShapeController.spline.SetRightTangent(1,dir1.normalized);
        ShapeController.spline.SetLeftTangent(1,-dir1.normalized);

        ShapeController.BakeMesh();
    }

    private void Complete() //Вызывается когда игла заканчивает движение
    {
        _isActive = false;
        _isComplete = true;
        NotificationCenter.postNotification(Notifications.Igla_Complete, null);
    }

    private void RotateUp()
    {
        
    }

    private void RotateDown()
    {
        
    }

    public void Activate() //Когда нужно активировать иглу для движения
    {
        _isActive = true;
    }

    public void Finish()
    {
        ThreadPiece.gameObject.SetActive(false);
        Igla.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

}
