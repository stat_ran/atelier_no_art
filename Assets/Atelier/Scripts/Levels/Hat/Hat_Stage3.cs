using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hat_Stage3 : BaseStage
{

   public Transform PrefabPoint;
   public Hat_Stage3_Rolik Rolik;
   
   public override void Init(BaseController baseController)
   {
      base.Init(baseController);
      Rolik.Init();
      base.baseController.BasePrefab.Activate(PrefabPoint);
   }
}
