﻿using UnityEngine;

public class Hat_Stage1_SewingNeedle : MonoBehaviour
{
   private Hat_Stage1 _scarfStage1;
   private Animator _animator;
   public bool _isKnot;
   private GameObject _knot;

   private void Awake()
   {
      _scarfStage1 = GetComponentInParent<Hat_Stage1>();
      _animator = GetComponent<Animator>();
      _animator.enabled = false;
   }

   public void SpawnStitch()
   {
      if (_isKnot)
      {
         _scarfStage1.InstantiateStitch();
         Destroy(_knot);
      }
   }

   public void StopAnim()
   {
      _animator.enabled = false;
   }

   private void OnTriggerEnter2D(Collider2D coll)
   {
      _isKnot = coll.CompareTag("Knot");
      _knot = coll.gameObject;
   }
   
   private void OnTriggerExit2D(Collider2D coll)
   {
      _isKnot = !coll.CompareTag("Knot");
      _knot = null;
   }

   private void OnMouseDown()
   {
      _animator.enabled=true;
   }
}
