using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hat_Stage4 : BaseStage
{
    public Transform PrefabPoint;
    public GameObject ButtonNext;
    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        base.baseController.BasePrefab.Activate(PrefabPoint);
        ButtonNext.SetActive(false);
        NotificationCenter.addCallback(Notifications.Get_Accessuary, ActivateButton);
    }

    private void ActivateButton(object obj)
    {
        ButtonNext.SetActive(true);
    }

    public void Next()
    {
        Complete();
    }

    public override void Complete()
    {
        base.Complete();
        ButtonNext.SetActive(false);
        NotificationCenter.removeCallbeck(Notifications.Get_Accessuary, ActivateButton);
    }
}
