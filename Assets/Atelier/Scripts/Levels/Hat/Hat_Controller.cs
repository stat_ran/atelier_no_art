using UnityEngine;

public class Hat_Controller : BaseController
{
    [HideInInspector] public HatPrefab Prefab;

    public override void Start()
    {
        base.Start();
        Type = DepartmentType.Hat;
    }

    public override void SetPrefab(BaseClothesPrefab clothesPrefab)
    {
        base.SetPrefab(clothesPrefab);
        Prefab=BasePrefab as HatPrefab;
    }
}