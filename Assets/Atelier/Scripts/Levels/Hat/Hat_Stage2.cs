using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.U2D;

public class Hat_Stage2 : BaseStage
{
    public Hat_Stage2_Igla Igla;
    public GameObject ThreadPrefab;
    [HideInInspector] private Vector2 _startPointThread;
    public Transform UpPolotno;
    public Transform DownPolotno;
    public SpriteRenderer CutPolotnoUp;
    public SpriteRenderer CutPolotnoDown;
    public Transform CurveTread;

    private Hat_Controller _controller;
    private float _height;
    private float _width;
    public bool IsUp;
    private int _number;
    private GameObject _middleThread;
    private List<SpriteRenderer> _verticalTreads = new List<SpriteRenderer>();
    private List<SpriteRenderer> _diagTreads = new List<SpriteRenderer>();
    private List<Scarf_Stage2_Thread> _treads = new List<Scarf_Stage2_Thread>();

    public Vector2 StartPointThread
    {
        get
        {
            return _startPointThread + _number * _width * Vector2.right;
        }
        
        set => _startPointThread = value;
    }

    public override void Start()
    {
        base.Start();
        _number = 1;
        IsUp = true;
        _controller = baseController.GetComponent<Hat_Controller>();
        NotificationCenter.addCallback(Notifications.Igla_Complete, IglaComplete);
        UpPolotno.GetComponent<SpriteRenderer>().color = _controller.Prefab.Colors[0];
        DownPolotno.GetComponent<SpriteRenderer>().color = _controller.Prefab.Colors[1];
        CutPolotnoDown.color = _controller.Prefab.Colors[1];
        CutPolotnoUp.color = _controller.Prefab.Colors[0];
        _width = CutPolotnoUp.size.y * 2f;
        _height = UpPolotno.position.y - DownPolotno.position.y + _width/2f;
        StartPointThread = new Vector2(-CutPolotnoUp.size.x / 2f - _width / 2f, CutPolotnoUp.size.y / 2f);
        GameObject obj = InstantiateThread(StartPointThread, StartPointThread + Vector2.down * _height, 3);
        _verticalTreads.Add(obj.GetComponentInChildren<SpriteRenderer>());
        CompleteUp();
        Igla.ThreadPiece.color = _controller.Prefab.Colors[0];
        Igla.GetComponent<SpriteShapeRenderer>().color = _controller.Prefab.Colors[0];
    }

    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.Igla_Complete, IglaComplete);
    }

    public void IglaComplete(object obj)
    {
        if (IsUp)
        {
            IsUp = false;
            CompleteDown();
        }
        else
        {
            IsUp = true;
            CompleteUp();
            
        }
    }

    private void CompleteUp()
    {
        //TODO Инитим иглу в верхней позиции
        Igla.InitThreadUp();
        //TODO Спамим нить серединную
        _middleThread = InstantiateThread(StartPointThread + Vector2.down * _height,
            StartPointThread + (_width/2f) * Vector2.right+Vector2.up*(_width/4f), 10);
        //TODO Устаниваливаем кривую нить в конец серединной нити
        CurveTread.position = (Vector2)UpPolotno.position + StartPointThread + (_width / 2f) * Vector2.right + _width / 4f * Vector2.up;
        UpPolotno.DOMove((Vector2)UpPolotno.position + Vector2.left * _width, 0.2f).SetEase(Ease.Linear);
        DownPolotno.DOMove((Vector2)DownPolotno.position + Vector2.left * _width, 0.2f).SetEase(Ease.Linear);
    }

    private void CompleteDown()
    {
        _number++;
       if(_number==11) CompletePolotno();
        //TODO Инитим иглу в нижней позиции
        Igla.InitThreadDown();
        //TODO правим серединную линию
        _middleThread.GetComponent<Scarf_Stage2_Thread>().MiddleLine(Vector2.right*(_width/2f)-Vector2.up*(_width/4f));
        _diagTreads.Add(_middleThread.GetComponentInChildren<SpriteRenderer>());
        //TODO Спамим нить вертикальную
        _middleThread = InstantiateThread(StartPointThread,
            StartPointThread + Vector2.down * _height, 3);
        _verticalTreads.Add(_middleThread.GetComponentInChildren<SpriteRenderer>());
        //TODO Устаниваливаем кривую нить в конец серединной нити
        CurveTread.position = (Vector2)UpPolotno.position + StartPointThread + Vector2.down * _height + _width / 4f * Vector2.down;
    }

    public GameObject InstantiateThread(Vector2 firstPoint, Vector2 secondPoint, int sortingOrder)
    { 
        GameObject obj = Instantiate(ThreadPrefab, UpPolotno);
        Scarf_Stage2_Thread thread = obj.GetComponent<Scarf_Stage2_Thread>();
       thread.Init(firstPoint, secondPoint,_controller.Prefab.Colors[0], sortingOrder);
       _treads.Add(thread);
       return obj;
    }

    public void CompletePolotno()
    {
        Igla.Finish();
        const float speed = 0.5f;
        UpPolotno.DOMove(new Vector3(0, UpPolotno.position.y, UpPolotno.position.z), speed).SetEase(Ease.Linear);
        DownPolotno.DOMove(new Vector3(0, DownPolotno.position.y, DownPolotno.position.z), speed).SetEase(Ease.Linear).onComplete+=Tighten;
    }

    private void Tighten()
    {
        StartCoroutine(Tighten_Cor());
    }

    IEnumerator Tighten_Cor()
    {
        var speed = 1f;
        while (true)
        {
            var delta = speed * Time.deltaTime;
            UpPolotno.Translate(delta * Vector2.down);
            DownPolotno.Translate(delta * Vector2.down);
            foreach (var thread in _treads)
            {
                thread.Tighten(delta);
            }
            yield return null;
            if (UpPolotno.position.y<0)
            {
                Complete();
                yield break;
            }
        }
    }
}
