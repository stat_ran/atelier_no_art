using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using DG.Tweening;
using UnityEngine;

public class Puffer_Stage1 : BaseStage
{
    public GameObject Rulon;
    public Transform TargetPointRulon;
    public ShelvesParent Shelve;

    
    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        NotificationCenter.addCallback(Notifications.Get_Color, StartRulonAnim);
    }
    
    public void StartRulonAnim(object obj)
    {
        Rulon.GetComponent<SpriteRenderer>().color = baseController.BasePrefab.ColorForPrefab.Color;
        Rulon.SetActive(true);
        Shelve.MoveToBack();
    }

    public void FinishRulon()
    {
        PlayCutterScene();
    }

    private void PlayCutterScene()
    {
        Complete();
    }

    public override void Complete()
    {
        base.Complete();
        NotificationCenter.removeCallbeck(Notifications.Get_Color, StartRulonAnim);
    }
}
