﻿using UnityEngine;

public class Cutter : InputObject
{
    public Transform Nodes;
    private Transform[] _nodes;
    private int _nextNode = 1;
    private Paint _paint;
    private Vector2 _prevPosition;
    private Animator _animator;

    public override void Start()
    {
        base.Start();
        
        _nodes = new Transform[Nodes.childCount];
        for (int i = 0; i < Nodes.childCount; i++)
            _nodes[i] = Nodes.GetChild(i);

        IsMovementAllowed = false;
        Transform.position = _nodes[0].position;
        _paint = GetComponent<Paint>();
        _prevPosition = Transform.position;
        _animator = GetComponentInChildren<Animator>();
        Vector2 nodeDirection = _nodes[1].position - Transform.position;
        Transform.rotation=Quaternion.FromToRotation(Vector3.up, nodeDirection);
    }
    
    public override void TouchDrag()
    {
        if (SwipeDirection != Vector2.zero)
        {
            Vector2 nodeDirection = _nodes[_nextNode].position - Transform.position;
            float tLen = Mathf.Clamp01(Vector2.Dot(nodeDirection, SwipeDirection) / nodeDirection.magnitude / nodeDirection.magnitude);
            
            Transform.position = Vector3.Lerp(Transform.position, _nodes[_nextNode].position, tLen);
            
            if (tLen == 1)
            {
                if (_nextNode == _nodes.Length - 1)
                    Finish(); 
                _nextNode++;
            }

         
            _paint.DrawLine(_prevPosition, Transform.position);
             
            _animator.SetBool("Cutter", true);
            Transform.rotation=Quaternion.FromToRotation(Vector3.up, nodeDirection);
            _prevPosition = Transform.position;
        }
        else
        {
            _animator.SetBool("Cutter", false);
        }
    }

    public override void TouchUp()
    {
        base.TouchUp();

        _animator.SetBool("Cutter", false);
    }

    private void Finish()
    {
        _animator.SetBool("Cutter", false);
        gameObject.SetActive(false);
        NotificationCenter.postNotification(Notifications.Cutter_End, null);
    }
}
