﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RulonCutter : MonoBehaviour
{
    public SpriteRenderer Tkan;
    public SpriteRenderer Wikroyka;
    public Transform HolstTransform;
    public Transform TargetPoint;

    private float _speed=0.5f;
    private AnimationCurve Curve = AnimationCurve.Linear(0, 0, 1f, 1f);


    public void Init(Color color)
    {
        Tkan.color = color;
        Wikroyka.color = color;
        GetComponentInChildren<Paint>().Init();
    }

    private void Start()
    {
        
    }

    public void StartAnimCutter()
    {
        HolstTransform.DOMove(TargetPoint.position, _speed).SetEase(Curve);
    }
}
