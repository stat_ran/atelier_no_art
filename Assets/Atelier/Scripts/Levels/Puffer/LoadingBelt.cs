﻿using System.Collections.Generic;
using UnityEngine;

public class LoadingBelt : InputObject
{
    public List<Transform> Items = new List<Transform>();
    private Transform[] _shelves;
    private int _upShelf;
    private int _downShelf;
    private int _upItem;
    private int _downItem;
    private float _maxPos;
    private float _minPos;
    private InputObject _currentItem;

    public override void Start()
    {
        base.Start();

        IsMovementAllowed = false;

        _shelves = new Transform[Transform.childCount];
        for (int i = 0; i < Transform.childCount; i++)
        {
            _shelves[i] = Transform.GetChild(i);
            SetItem(_shelves[i], Transform.childCount - 1 - i);
        }

        foreach (Transform item in Items)
        {
            SpriteRenderer sp = item.GetComponent<SpriteRenderer>();
            sp.sortingLayerName = "ActiveTools";
            sp.sortingOrder = 0;
            sp.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            if (item.childCount > 0)
            {
                sp = item.GetChild(0).GetComponent<SpriteRenderer>();
                sp.sortingLayerName = "ActiveTools";
                sp.sortingOrder = 1;
                sp.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            }
        }

        _upItem = 0;
        _downItem = _shelves.Length - 1;

        _upShelf = _shelves.Length - 1;
        _downShelf = 0;
        float spriteYSize = _shelves[_upShelf].GetComponent<SpriteRenderer>().sprite.bounds.size.y;
        _maxPos = _shelves[_upShelf].localPosition.y + spriteYSize;
        _minPos = _shelves[_downShelf].localPosition.y;

        NotificationCenter.addCallback("Push_Item", PushItem);
        NotificationCenter.addCallback("Item_Back", PullItem);
    }

    void Update()
    {
        if (_currentItem)
        {
            if (!_currentItem.IsMovementAllowed)
            {
                if (Vector2.Distance(_currentItem.PrevPos, (Vector2)_currentItem.Transform.position) > 2.0f)
                    _currentItem.IsMovementAllowed = true;
            }
        }
        else
        {
            Shifting(IsDown ? SwipeDirection.y : Time.deltaTime);
        }
    }

    private void Shifting(float offset)
    {
        for (int i = 0; i < _shelves.Length; i++)
        {
            _shelves[i].position += offset * Vector3.up;
        }

        if (offset > 0 & _shelves[_upShelf].localPosition.y >= _maxPos)
        {
            _shelves[_upShelf].localPosition = new Vector3(0, _minPos + _shelves[_upShelf].localPosition.y - _maxPos, 0);
            _upItem = (_upItem + 1) % Items.Count;
            _downItem = (_downItem + 1) % Items.Count;
            SetItem(_shelves[_upShelf], _downItem);
            _downShelf = _upShelf;
            _upShelf = (_upShelf + _shelves.Length - 1) % _shelves.Length;
        }
        else if (offset < 0 & _shelves[_downShelf].localPosition.y <= _minPos)
        {
            _shelves[_downShelf].localPosition = new Vector3(0, _maxPos + _shelves[_downShelf].localPosition.y - _minPos, 0);
            _upItem = (_upItem + Items.Count - 1) % Items.Count;
            _downItem = (_downItem + Items.Count - 1) % Items.Count;
            SetItem(_shelves[_downShelf], _upItem);
            _upShelf = _downShelf;
            _downShelf = (_downShelf + 1) % _shelves.Length;
        }
    }

    private void SetItem(Transform shelf, int itemNumber)
    {
        if (shelf.childCount > 0)
            shelf.GetChild(0).gameObject.SetActive(false);
        Items[itemNumber].gameObject.SetActive(true);
        Items[itemNumber].SetParent(shelf);
        Items[itemNumber].localPosition = new Vector3(0, -0.6f, -1); // Hard code!
    }

    private void PushItem(object obj)
    {
        if (Items.Contains((Transform)obj))
        {
            _currentItem = ((Transform)obj).GetComponent<InputObject>();
            _currentItem.IsMovementAllowed = false;
        }
    }

    private void PullItem(object obj)
    {
        _currentItem = null;
    }

    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck("Push_Item", PushItem);
        NotificationCenter.removeCallbeck("Item_Back", PullItem);
    }
}
