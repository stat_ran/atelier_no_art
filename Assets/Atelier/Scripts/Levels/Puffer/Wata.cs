﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Wata : InputObject
{
    public Transform TargetPoint;
    public Transform PufferPoint;
    public AnimationCurve Curve;
    
    private SpriteRenderer _spriteRenderer;
    private float _distTarget=5f;
    private float _speed=0.3f;
    private Puffer_Stage3 _controller;


    public override void Start()
    {
        base.Start();
        _controller = FindObjectOfType<Puffer_Stage3>();
    }

    public override void TouchUp()
    {
        base.TouchUp();
       
        if (Vector2.Distance(Transform.position, TargetPoint.position) < _distTarget)
        {
            _controller.AnimPuffer();
            Transform.DOMove(PufferPoint.position, _speed).SetEase(Curve).onComplete+=MoveToTargetComplete;
        }
        else
        {
            Transform.DOMove(FirstGlobalPosition, _speed).SetEase(Curve);
        }
    }

    public override void TouchDown()
    {
        base.TouchDown();
    }

    private void MoveToTargetComplete()
    {
        _controller.CountCotton--;
        if (_controller.CountCotton <= 0)
        {
            _controller.EndPufferScene();
        }

        gameObject.SetActive(false);
    }
    
}
