using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class Puffer_Stage4 : BaseStage
{

    public Transform PrefabPoint;
    public GameObject NextButton;
    
    private Puffer_Controller _controller;
    
    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        _controller=base.baseController as Puffer_Controller;
        NextButton.SetActive(false);
        _controller.Prefab.Activate(PrefabPoint);
        NotificationCenter.addCallback(Notifications.Get_Accessuary, GetAccessuar);
    }

    private void GetAccessuar(object obj)
    {
        Debug.Log("Puffer_Stage4");
        NextButton.SetActive(true);
    }

    public void Next()
    {
        Complete();
    }

    public override void Complete()
    {
        NotificationCenter.removeCallbeck(Notifications.Get_Accessuary, GetAccessuar);
        base.Complete();
    }
}
