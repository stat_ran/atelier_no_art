using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Puffer_Stage3 : BaseStage
{
    
    public int CountCotton=10;
    public GameObject Bascet;

    public Puffer_Stage3_Hole Hole1;
    public Puffer_Stage3_Hole Hole2;
    public Puffer_Stage3_Hole Hole3;
    public Puffer_Stage3_Hole Hole4;
    
    private GameObject PufferHole;
    private GameObject PufferHoleBack;
    
    public GameObject PufferFull;

    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        StartPufferScene();
    }

    private void StartPufferScene()
    {
        Hole1.gameObject.SetActive(false);
        Hole2.gameObject.SetActive(false);
        Hole3.gameObject.SetActive(false);
        Hole4.gameObject.SetActive(false);
        
        for (int i = 0; i < Data.Departments[baseController.Type].Prefabs.Length; i++)
        {
            if (Data.Departments[baseController.Type].Prefabs[i].name == baseController.BasePrefab.name)
            {
                switch (i)
                {
                    case 0:
                    {
                        PufferHole = Hole1.Front;
                        PufferHoleBack = Hole1.Back;
                        Hole1.gameObject.SetActive(true);
                        break;
                    }
                    case 1:
                    {
                        PufferHole = Hole2.Front;
                        PufferHoleBack = Hole2.Back;
                        Hole2.gameObject.SetActive(true);
                        break;
                    }
                    case 2:
                    {
                        PufferHole = Hole3.Front;
                        PufferHoleBack = Hole3.Back;
                        Hole3.gameObject.SetActive(true);
                        break;
                    }
                    case 3:
                    {
                        PufferHole = Hole4.Front;
                        PufferHoleBack = Hole4.Back;
                        Hole4.gameObject.SetActive(true);
                        break;
                    }
                }
            }
        }
        
        //PufferFull.SetActive(false);
        PufferHole.SetActive(true);
        Bascet.SetActive(true);
        //PufferFull.GetComponent<SpriteRenderer>().color = baseController.BasePrefab.ColorForPrefab.Color;
        PufferHole.GetComponent<SpriteRenderer>().color = baseController.BasePrefab.ColorForPrefab.Color;
        PufferHoleBack.GetComponent<SpriteRenderer>().color = baseController.BasePrefab.ColorForPrefab.Color;
        Bascet.transform.position = DownRight;
        //TODO создать сцену
        
  
       
        //Когда весь пух будет в куртке куртка меняется на целую, корзина уезжает
        //Загружаем следующую сцену с этой курткой
    }

    public void AnimPuffer()
    {
        PufferHole.GetComponent<Animator>().Play("Anim");
    }

    public void EndPufferScene()
    {
        StartCoroutine(EndPufferScene_Cor());
    }

    IEnumerator EndPufferScene_Cor()
    {
        Bascet.transform.DOMove((Vector2)Bascet.transform.position + Vector2.right * 10f, 0.3f)
            .SetEase(AnimationCurve.Linear(0, 0, 1, 1));
        //PufferFull.SetActive(true);
        baseController.BasePrefab.Activate(PufferFull.transform);
        PufferFull.transform.DOMove(Vector3.zero, 0.2f).SetEase(AnimationCurve.Linear(0, 0, 1, 1));
        PufferHole.SetActive(false);
        yield return new WaitForSeconds(1f);
        Complete();
    }
}
