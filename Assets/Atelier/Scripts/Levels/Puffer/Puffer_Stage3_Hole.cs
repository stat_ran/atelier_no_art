using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;

public class Puffer_Stage3_Hole : BaseBehavior
{
    public int Number;
    public GameObject Front;
    public GameObject Back;
}
