using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puffer_Controller : BaseController
{
    [HideInInspector] public PufferPrefab Prefab;

    public override void Awake()
    {
        base.Awake();
        Type = DepartmentType.Puffer;
    }

    public override void SetPrefab(BaseClothesPrefab clothesPrefab)
    {
        base.SetPrefab(clothesPrefab);
        Prefab=BasePrefab as PufferPrefab;
    }
    
    
    
}
