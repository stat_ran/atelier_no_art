﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PufferAnimFinishCutter : MonoBehaviour
{
    public void EndAnim()
    {
        FindObjectOfType<Puffer_Stage2>().ShowFinishPuffer();
    }
}
