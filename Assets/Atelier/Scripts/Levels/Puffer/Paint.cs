﻿using System;
using System.Diagnostics.Tracing;
using UnityEngine;

public class Paint : MonoBehaviour
{

	public SpriteRenderer[] SpriteRenderers; // массив исходных спрайтов для рисования
	public int Radius = 20;
	public Color _brushColor = Color.clear;
	public Texture2D[] Masks;
	public Color MaskColor = Color.white;
	private int _maskIndex = 0;
	private int _maskWidth;
	private int _maskHeight;
	private Texture2D _texture2D;

	public RenderTexture RenderTexture;


	private void Start()
	{
		Init();
	}

	public void Init()
	{
		Texture2D[] texture2D =new Texture2D[SpriteRenderers.Length];
		Sprite[] currentSprite =new Sprite[SpriteRenderers.Length];
		
		for (int i = 0; i < SpriteRenderers.Length; i++)
		{
			texture2D[i]=new Texture2D(SpriteRenderers[i].sprite.texture.width, SpriteRenderers[i].sprite.texture.height);
			texture2D[i].SetPixels(SpriteRenderers[i].sprite.texture.GetPixels());
			texture2D[i].Apply();
			currentSprite[i] = Sprite.Create(texture2D[i], new Rect(0, 0, texture2D[i].width, texture2D[i].height), Vector2.one/2, SpriteRenderers[i].sprite.pixelsPerUnit);
			SpriteRenderers[i].sprite = currentSprite[i];
		}
	}


	public void Draw(Vector2 brushPoint) // рисование (клонирование кисти)
	{
		for (int s = 0; s < SpriteRenderers.Length; s++)
		{
			Sprite sprite = SpriteRenderers[s].sprite;
			Transform spTransform = SpriteRenderers[s].transform;

			Vector2 point = Solving(spTransform.right, spTransform.up, brushPoint - (Vector2)spTransform.position);
			Vector2 pointClick = new Vector2(point.x / spTransform.localScale.x * sprite.pixelsPerUnit + sprite.texture.width / 2,
				point.y / spTransform.localScale.y * sprite.pixelsPerUnit + sprite.texture.height / 2);

			for (int i = -Radius; i < Radius; i++)
			{
				for (int j = -Radius; j < Radius; j++)
				{
					if (Vector2.Distance(Vector2.zero, new Vector2(i, j)) > Radius) continue;
					sprite.texture.SetPixel((int)pointClick.x + i, (int)pointClick.y + j, _brushColor);
				}
			}
			
			sprite.texture.Apply();
		}
	}

	public void DrawLine(Vector2 start, Vector2 end) // рисование (клонирование кисти)
	{
		for (int s = 0; s < SpriteRenderers.Length; s++)
		{
			Sprite sprite = SpriteRenderers[s].sprite;
			Transform spTransform = SpriteRenderers[s].transform;
			
			//_texture2D=new Texture2D(sprite.texture.width, sprite.texture.height, TextureFormat.RGBA32, true);
			//_texture2D.SetPixels(sprite.texture.GetPixels());

			

			Vector2 startLocal = Solving(spTransform.right, spTransform.up, start - (Vector2)spTransform.position);
            Vector2 endLocal = Solving(spTransform.right, spTransform.up, end - (Vector2)spTransform.position);

			Vector2 pointClickStart = new Vector2(startLocal.x / spTransform.lossyScale.x * sprite.pixelsPerUnit + sprite.texture.width / 2,
				startLocal.y / spTransform.lossyScale.y * sprite.pixelsPerUnit + sprite.texture.height / 2);
			Vector2 pointClickEnd = new Vector2(endLocal.x / spTransform.lossyScale.x * sprite.pixelsPerUnit + sprite.texture.width / 2,
				endLocal.y / spTransform.lossyScale.y * sprite.pixelsPerUnit + sprite.texture.height / 2);

            Vector2 direct = pointClickEnd - pointClickStart;
			for (int k = 1; k < direct.magnitude + 1; k++)
			{
				Vector2 vec = pointClickStart + direct.normalized * k;
				for (int i = -Radius; i < Radius; i++)
				{
					for (int j = -Radius; j < Radius; j++)
					{
						if(Vector2.Distance(Vector2.zero, new Vector2(i,j))>Radius) continue;
						sprite.texture.SetPixel((int) vec.x + i, (int) vec.y + j, _brushColor);
					}
				}
			}

			sprite.texture.Apply();
		}
	}

	public void DrawMask(Vector2 maskPoint)
    {
		for (int s = 0; s < SpriteRenderers.Length; s++)
		{
			Sprite sprite = SpriteRenderers[s].sprite;
			Transform spTransform = SpriteRenderers[s].transform;

			Vector2 point = Solving(spTransform.right, spTransform.up, maskPoint - (Vector2)spTransform.position);
			Vector2 pointClick = new Vector2(point.x / spTransform.localScale.x * sprite.pixelsPerUnit + sprite.texture.width / 2,
											 point.y / spTransform.localScale.y * sprite.pixelsPerUnit + sprite.texture.height / 2);
			Rect rectMask = new Rect(pointClick.x - Masks[_maskIndex].width / 2,
									 pointClick.y - Masks[_maskIndex].height / 2,
									 Masks[_maskIndex].width,
									 Masks[_maskIndex].height);

			if (!rectMask.Overlaps(sprite.rect))
				continue;
			Vector4 shift = Vector4.zero;
			if (!sprite.rect.Contains(rectMask.min) || !sprite.rect.Contains(rectMask.max))
			{
				shift = new Vector4(rectMask.x < 0 ? rectMask.x : 0,
									rectMask.y < 0 ? rectMask.y : 0,
									rectMask.xMax > sprite.rect.xMax ? rectMask.xMax - sprite.rect.xMax : 0,
									rectMask.yMax > sprite.rect.yMax ? rectMask.yMax - sprite.rect.yMax : 0);
				rectMask.Set(rectMask.x - shift.x,
							 rectMask.y - shift.y,
							 rectMask.width - shift.z + shift.x,
							 rectMask.height - shift.w + shift.y);
			}

			Color[] msPixels = Masks[_maskIndex].GetPixels(-(int)shift.x, -(int)shift.y, (int)rectMask.width, (int)rectMask.height);
			Color[] spPixels = sprite.texture.GetPixels((int)rectMask.xMin, (int)rectMask.yMin, (int)rectMask.width, (int)rectMask.height);
			for (int i = 0; i < msPixels.Length; i++)
				if (msPixels[i].a != 0)
					spPixels[i] = Color.Lerp(spPixels[i], msPixels[i] * MaskColor, 0.5f);

			sprite.texture.SetPixels((int)rectMask.xMin, (int)rectMask.yMin, (int)rectMask.width, (int)rectMask.height, spPixels);

            sprite.texture.Apply();
		}
		_maskIndex = (_maskIndex + 1) % Masks.Length;
	}

	private Vector2 Solving(Vector2 right, Vector2 up, Vector2 pos)
    {
		float valX = (up.x * pos.y - up.y * pos.x) / (up.x * right.y - up.y * right.x);
		float valY = (right.x * pos.y - right.y * pos.x) / (right.x * up.y - right.y * up.x);
		return new Vector2(valX, valY);
    }

	public void SaveImage(Sprite fromSprite)
	{
		
	}
}