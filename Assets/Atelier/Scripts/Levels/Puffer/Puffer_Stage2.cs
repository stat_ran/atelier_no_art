using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puffer_Stage2 : BaseStage
{

    public RulonCutter Rulon1_1;
    public RulonCutter Rulon1_2;
    public RulonCutter Rulon2_1;
    public RulonCutter Rulon2_2;
    public RulonCutter Rulon3_1;
    public RulonCutter Rulon3_2;
    public RulonCutter Rulon4_1;
    public RulonCutter Rulon4_2;
    
    private RulonCutter RulonCutter1; 
    private RulonCutter RulonCutter2;
    public GameObject FinishAction;
    private SpriteRenderer FinishPuffer;
    private bool _isSecond = false;
    private GameObject _rulonCutter;

    public SpriteRenderer Finish1;
    public SpriteRenderer Finish2;
    public SpriteRenderer Finish3;
    public SpriteRenderer Finish4;


    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        Rulon1_1.gameObject.SetActive(false);
        Rulon1_2.gameObject.SetActive(false);
        Rulon2_1.gameObject.SetActive(false);
        Rulon2_2.gameObject.SetActive(false);
        Rulon3_1.gameObject.SetActive(false);
        Rulon3_2.gameObject.SetActive(false);
        Rulon4_1.gameObject.SetActive(false);
        Rulon4_2.gameObject.SetActive(false);
        for (int i = 0; i < Data.Departments[baseController.Type].Prefabs.Length; i++)
        {
            if (Data.Departments[baseController.Type].Prefabs[i].name == baseController.BasePrefab.name)
            {
                switch (i)
                {
                    case 0:
                    {
                        RulonCutter1 = Rulon1_1;
                        RulonCutter2 = Rulon1_2;
                        FinishPuffer = Finish1;
                        break;
                    }
                    case 1:
                    {
                        RulonCutter1 = Rulon2_1;
                        RulonCutter2 = Rulon2_2;
                        FinishPuffer = Finish2;
                        break;
                    }
                    case 2:
                    {
                        RulonCutter1 = Rulon3_1;
                        RulonCutter2 = Rulon3_2;
                        FinishPuffer = Finish3;
                        break;
                    }
                    case 3:
                    {
                        RulonCutter1 = Rulon4_1;
                        RulonCutter2 = Rulon4_2;
                        FinishPuffer = Finish4;
                        break;
                    }
                }
            }
        }
        NotificationCenter.addCallback(Notifications.Cutter_End, CutterEnd);
        StartCutterScene();
    }


    private void StartCutterScene()
    {
        RulonCutter1.gameObject.SetActive(true);
        RulonCutter1.Init(baseController.BasePrefab.ColorForPrefab.Color);
    }

    IEnumerator AnimRulonCutter1()
    {
        RulonCutter1.GetComponent<RulonCutter>().StartAnimCutter();
        yield return new WaitForSeconds(2f);
        SecondCutterScene();
    }
    
    IEnumerator AnimRulonCutter2()
    {
        RulonCutter2.GetComponent<RulonCutter>().StartAnimCutter();
        yield return new WaitForSeconds(1f);
        FinishCutterScene();
    }

    private void CutterEnd(object obj)
    {
        if (!_isSecond)
        {
            StartCoroutine(AnimRulonCutter1());
            _isSecond = true;
        }
        else
        {
            StartCoroutine(AnimRulonCutter2());
        }
    }
    

    public void SecondCutterScene()
    {
        RulonCutter1.gameObject.SetActive(false);
        RulonCutter2.gameObject.SetActive(true);
        RulonCutter2.Init(baseController.BasePrefab.ColorForPrefab.Color);
    }

    public void FinishCutterScene()
    {
        RulonCutter2.gameObject.SetActive(false);
        FinishAction.SetActive(true);
        foreach (var spriteRenderer in FinishAction.GetComponentsInChildren<SpriteRenderer>())
        {
            spriteRenderer.color = baseController.BasePrefab.ColorForPrefab.Color;
            spriteRenderer.sprite = RulonCutter1.Wikroyka.sprite;
        }
    }

    public void ShowFinishPuffer()
    {
        FinishAction.SetActive(false);
        FinishPuffer.color = baseController.BasePrefab.ColorForPrefab.Color;
        FinishPuffer.gameObject.SetActive(true);
        StartCoroutine(ShowFinishPuffer_Cor());
    }

    IEnumerator ShowFinishPuffer_Cor()
    {
        yield return new WaitForSeconds(1f);
        Complete();
    }

    public override void Complete()
    {
        base.Complete();
        NotificationCenter.removeCallbeck(Notifications.Cutter_End, CutterEnd);
    }
}
