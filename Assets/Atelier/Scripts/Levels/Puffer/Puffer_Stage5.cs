using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puffer_Stage5 : BaseStage
{
    public GameObject NextButton;
    public Transform PrefabPoint;
    
    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        NextButton.SetActive(false);
        base.baseController.BasePrefab.Activate(PrefabPoint);
        NotificationCenter.addCallback(Notifications.Get_Accessuary, GetAccess);
    }

    private void GetAccess(object obj)
    {
        NextButton.SetActive(true);
    }

    public void Next()
    {
        Complete();
    }

    public override void Complete()
    {
        base.Complete();
        NotificationCenter.removeCallbeck(Notifications.Get_Accessuary, GetAccess);
    }
}
