using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class Dress_Stage3 : BaseStage
{

    public Transform PointPrefab;
    public Transform ShelfParent;
    public GameObject NextButton;

    private DressPrefab _prefab;
    
    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        base.baseController.BasePrefab.Activate(PointPrefab);
        _prefab = base.baseController.BasePrefab as DressPrefab;
        _prefab.DecorsPoints[0].SpriteRendererPositions[0].sprite = null;
        NextButton.SetActive(false);
        Destroy(_prefab.SpriteRenderer.GetComponent<SpriteMask>());
        NotificationCenter.addCallback(Notifications.Get_Accessuary, ActivateNextButton);
    }

    private void ActivateNextButton(object obj)
    {
        NextButton.SetActive(true);
    }

    public void Next()
    {
        Complete();
    }
    

    public override void Complete()
    {
        base.Complete();
        NotificationCenter.removeCallbeck(Notifications.Get_Accessuary, ActivateNextButton);
    }
}
