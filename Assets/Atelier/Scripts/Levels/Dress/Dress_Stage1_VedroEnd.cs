using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dress_Stage1_VedroEnd : MonoBehaviour
{
   public void End()
   {
      GetComponentInParent<Dress_Stage1>().VedroEnd();
   }
}
