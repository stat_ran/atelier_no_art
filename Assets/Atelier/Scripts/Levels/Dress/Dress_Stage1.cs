using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Dress_Stage1 : BaseStage
{
    public Transform Bottle;
    public Transform VedroStruya;
    public Transform Vedro;
    public Animator Struya;
    public Animator Zalivka;
    public Transform ShelveParent;
    public SpriteRenderer KraskaNaVedre;

    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        Vedro.gameObject.SetActive(false);
        Struya.gameObject.SetActive(false);
        Zalivka.gameObject.SetActive(false);
        VedroStruya.gameObject.SetActive(true);
        Bottle.position = DownLeft + Vector2.left * 10f;
        
        Bottle.DOMove(DownLeft, 1f).SetEase(Ease.Linear);
        NotificationCenter.addCallback(Notifications.Get_Color,StartColor);
    }

    private void StartColor(object obj)
    {
        GetColor(baseController.BasePrefab.ColorForPrefab.Color);
        Vedro.gameObject.SetActive(true);
    }

    public void VedroEnd()
    {
        Struya.gameObject.SetActive(true);
        Zalivka.gameObject.SetActive(true);
        ShelveParent.DOMove(ShelveParent.position + Vector3.right * 10f, 1f).SetEase(Ease.Linear);
    }
    
    public void ZalivkaEnd()
    {
        Struya.gameObject.SetActive(false);
        Vedro.DOMove(Vedro.position + Vector3.right * 20f, 1f).SetEase(Ease.Linear).onComplete += Complete;
    }

    public override void Complete()
    {
        base.Complete();
        ShelveParent.DOKill();
        NotificationCenter.removeCallbeck(Notifications.Get_Color, StartColor);
    }

    private void GetColor(Color color)
    {
        Struya.GetComponent<SpriteRenderer>().color = color;
        KraskaNaVedre.color = color;
        Zalivka.GetComponent<SpriteRenderer>().color = color;
    }


}
