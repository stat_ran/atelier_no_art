using System;
using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using DG.Tweening;
using UnityEngine;

public class Dress_Stage2_Aerograf : BaseBehavior
{

    public SpriteRenderer Rezervuar;
    public GameObject BrushPrefab;
    public Transform RayCastPoint;

    public Transform Position1;
    public Transform Position2;
    public Transform Position3;
    public Transform Position4;
    
    private ParticleSystem _particle;
    private Dress_Stage2 _stage2;
    private Sprite _sprite;
    private Texture2D _texture2D;
    private Sprite _currentSprite;
    private float _timer;
    private BaseClothesPrefab _prefab;
    private List<GameObject> _prefabs = new List<GameObject>();
    private List<Vector2> _checkPoint = new List<Vector2>();
    private bool _isPause;

    private Color[] _colorsBrush;

    public void Init()
    {
        _particle = GetComponentInChildren<ParticleSystem>();
        _particle.Stop();
        _stage2 = GetComponentInParent<Dress_Stage2>();
        _prefab = _stage2.baseController.BasePrefab;
        Rezervuar.color = _prefab.ColorForPrefab.Color;
        SpriteMask mask = _prefab.SpriteRenderer.gameObject.AddComponent<SpriteMask>();
        mask.sprite = _prefab.SpriteRenderer.sprite;
        _prefab.SpriteRenderer.color=Color.white;
        ParticleSystem.MainModule main = _particle.main;
        ParticleSystem.MinMaxGradient gradient = main.startColor;
        gradient.color = new Color(_prefab.ColorForPrefab.Color.r - 0.2f, _prefab.ColorForPrefab.Color.g - 0.2f,
            _prefab.ColorForPrefab.Color.b - 0.2f, _prefab.ColorForPrefab.Color.a);
        main.startColor = gradient;
        InitCheckBrush();
    }

    private void Paint()
    {
        if(Time.time-_timer<0.05f) return;
        if(Vector2.Distance(_prefab.transform.position, RayCastPoint.position)>5.5f) return;
        GameObject obj = Instantiate(BrushPrefab);
        obj.transform.position = RayCastPoint.position;
        obj.GetComponent<SpriteRenderer>().color = _prefab.ColorForPrefab.Color;
        _prefabs.Add(obj);
        _timer = Time.time;
        CheckBrush(RayCastPoint.position);
    }

    private void InitCheckBrush()
    {
        _checkPoint.Add(_prefab.transform.position);
        _checkPoint.Add((Vector2)_prefab.transform.position+Vector2.right*2f);
        _checkPoint.Add((Vector2)_prefab.transform.position+Vector2.left*2f);
        _checkPoint.Add((Vector2)_prefab.transform.position+Vector2.up*2f);
        _checkPoint.Add((Vector2)_prefab.transform.position+Vector2.down*2f);
        _checkPoint.Add((Vector2)_prefab.transform.position+Vector2.right*2f + Vector2.up*2f);
        _checkPoint.Add((Vector2)_prefab.transform.position+Vector2.right*2f + Vector2.down*2f);
        _checkPoint.Add((Vector2)_prefab.transform.position+Vector2.left*2f + Vector2.up*2f);
        _checkPoint.Add((Vector2)_prefab.transform.position+Vector2.left*2f + Vector2.down*2f);
    }

    private void CheckBrush(Vector2 position)
    {
        if(_checkPoint.Count==0) return;
        foreach (var vector2 in _checkPoint)
        {
            Debug.Log(vector2 + " " + position);
            if (Vector2.Distance(vector2, position) < 1f)
            {
                _checkPoint.Remove(vector2);
                break;
            }
        }
    }

    private void StartFinal()
    {
        _isPause = true;
        _particle.Play();
        transform.DOMove(Position1.position, 0.5f).SetEase(Ease.Linear).onComplete += MoveTo2;
    }

    private void MoveTo2()
    {
        transform.DOMove(Position2.position, 0.5f).SetEase(Ease.Linear).onComplete += MoveTo3;
        foreach (var prefab in _prefabs)
        {
            Destroy(prefab);
        }

        _prefab.SpriteRenderer.color = _prefab.ColorForPrefab.Color;
    }
    
    private void MoveTo3()
    {
        transform.DOMove(Position3.position, 0.5f).SetEase(Ease.Linear).onComplete += MoveTo4;
    }
    
    private void MoveTo4()
    {
        transform.DOMove(Position3.position+Vector3.right*20f, 1f).SetEase(Ease.Linear).onComplete+=_stage2.Complete;
    }

    private void OnMouseDown()
    {
        transform.position = MousePosition;
        _particle.Play();
    }

    private void OnMouseDrag()
    {
        if(_isPause) return;
        transform.position = MousePosition;
        Paint();
    }

    private void OnMouseUp()
    {
        _particle.Stop();
        if(_checkPoint.Count<=1) StartFinal();
    }
    
}
