using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dress_Controller : BaseController
{
    [HideInInspector] public DressPrefab Prefab;

    public override void Awake()
    {
        base.Awake();
        Type = DepartmentType.Dress;
    }

    public override void SetPrefab(BaseClothesPrefab clothesPrefab)
    {
        base.SetPrefab(clothesPrefab);
        Prefab=BasePrefab as DressPrefab;
    }
}
