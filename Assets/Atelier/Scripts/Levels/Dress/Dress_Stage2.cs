using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dress_Stage2 : BaseStage
{
    public Transform PointPrefab;
    
    private Dress_Stage2_Aerograf _aerograf;
    private DressPrefab _prefab;

    public override void Init(BaseController baseController)
    {
        base.Init(baseController);
        _aerograf = GetComponentInChildren<Dress_Stage2_Aerograf>();
        base.baseController.BasePrefab.Activate(PointPrefab);
        _prefab = base.baseController.BasePrefab as DressPrefab;
        _prefab.DecorsPoints[0].SpriteRendererPositions[0].sprite = null;
        GameObject obj = Instantiate(_prefab.SpriteRenderer.gameObject, transform);
        obj.transform.position = _prefab.transform.position;
        SpriteRenderer sr = obj.GetComponent<SpriteRenderer>();
        sr.sortingLayerName = "Clothes";
        sr.sortingOrder = 20;
        sr.color=new Color(1f,1f,1f,0.15f);
        _aerograf.Init();
    }
}
