﻿
   public enum Scenes
   {
      Blouse,
      Boots,
      Dress,
      Hat,
      MainMenu,
      Pants,
      PufferJacket,
      Scarf
   }

   public enum DepartmentType
   {
      Puffer, //Пуховик
      Bottom, //Низ
      Scarf, //Шарф
      Dress, //Платье
      Upper, //Верх
      Shoes, //Обувь
      Hat, //Шапка
   }

   public enum TypeDecor : int
   {
      First = 0,
      Second = 1,
      Third = 2
   }

   public enum Direction
   {
      Left,
      Right,
      Up,
      Down
   }

   public enum AnimName
   {
      Walk_Start,
      Walk_Buy,
      Walk_Sad,
      Hi,
      Think,
      Speak,
      Glad,
      Sad
   }

   // public enum ClothesName
   // {
   //    Scarf_1, Scarf_2, Scarf_3, Scarf_4, Scarf_5, Scarf_6, Scarf_7, Scarf_8,
   //    Puffer_1, Puffer_2, Puffer_3, Puffer_4,
   //    Pants_1, Pants_2, Pants_3, Pants_4,
   //    Dress_1, Dress_2, Dress_3, Dress_4,
   //    Upper_1, Upper_2,
   // }

