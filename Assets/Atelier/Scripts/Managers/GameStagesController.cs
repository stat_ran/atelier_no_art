﻿using System;
using System.Collections.Generic;
using AceCream.Scripts.Template.Tools;
using Atelier.Scripts;
using UnityEngine;

namespace AceCream.Scripts.Template
{
    public class GameStagesController: BaseBehavior
    {
        [Serializable]
        public class GameObjectContainer
        {
            [SerializeField] private List<GameObject> _list;
            public List<GameObject> List => _list;
        }
        
        [SerializeField] private int _currentStage;
        [SerializeField] private bool _test;
        [SerializeField] private float _delay = 1f;
        [SerializeField] private List<GameObjectContainer> _disableOnChange;
        [SerializeField] private List<GameObject> _stageParent;
        
        public override void Start()
        {
            SetStage(_currentStage);
        }

        public void PlayNextStage()
        {
            _currentStage++;
            Timer.Register(_delay, () =>
            {
                SetStage(_currentStage);
            });
        }
        
        public void _PlayNextStage() => PlayNextStage();
        
        private void SetStage(int index)
        {
            if (index > 0)
            {
                _disableOnChange[index - 1].List.ForEach(i => i?.SetActive(false));
            }
            
            _stageParent.ForEach(i => i?.SetActive(false));
            _stageParent[index].SetActive(true);
        }
    }
}