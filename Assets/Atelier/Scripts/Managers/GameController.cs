﻿using System;
using System.Collections.Generic;
using AceCream.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Atelier.Scripts
{
    public class GameController : MonoBehaviour
    {
        public enum GameState: byte
        {
            None,
            Initialization,
            Run,
            Pause,
            Exit,
        }

        public static GameController Instance;
        
        [HideInInspector] public Pers PersCurrent;
        [HideInInspector] public Vector2 PointClick;
        [HideInInspector] public string NamePrefab_PhotoFrame;
        [HideInInspector] public DepartmentType DepartmentType_Photo;
        
        [SerializeField] private bool _dontDestroyOnLoad;
        
        public DataContainer dataContainer;
        [HideInInspector] public int NumberPers;
        [HideInInspector] public bool IsBuy;

        public Vector2 Resolution { get; private set; }
        
        public GameState State { get; private set; }

        private GameCamera _gameCamera;
        public GameCamera GameCamera
        {
            get
            {
                if (_gameCamera == null)
                    _gameCamera = FindObjectOfType<GameCamera>();
                return _gameCamera;
            }
        }
        
        private InputManager _inputManager;
    
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                if (gameObject != Instance.gameObject)
                    Destroy(gameObject);
            }

            NumberPers = -1;
            Resolution = new Vector2(Screen.width, Screen.height);
            State = GameState.Initialization;
            Initialize();
            _inputManager = InputManager.Instance;
            NamePrefab_PhotoFrame = "";
        }

        public void Init()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                if (gameObject != Instance.gameObject)
                    Destroy(gameObject);
            }

            NumberPers = -1;
            Resolution = new Vector2(Screen.width, Screen.height);
            State = GameState.Initialization;
            Initialize();
            _inputManager = InputManager.Instance;
            NamePrefab_PhotoFrame = "";
        }

        private void Initialize()
        {
            State = GameState.Run;
        }

        private void Update() //TODO: REMOVE, tests only
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                Debug.Log("F1");
                Time.timeScale = Time.timeScale > 2f ? Time.timeScale = 1f : Time.timeScale = 5f;
            }
        }
        
        public void StartScene(DepartmentType departmentType)
        {
            switch (departmentType)
            {
                case DepartmentType.Scarf:
                    SceneManager.LoadScene(DepartmentType.Scarf.ToString());
                    break;
                case DepartmentType.Puffer:
                    SceneManager.LoadScene(DepartmentType.Puffer.ToString());
                    break;
                case DepartmentType.Hat:
                    SceneManager.LoadScene(DepartmentType.Hat.ToString());
                    break;
                case DepartmentType.Dress:
                    SceneManager.LoadScene(DepartmentType.Dress.ToString());
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(departmentType), departmentType, null);
            }
        }

        public GameObject GetSavedPrefab(DepartmentType departmentType, string namePrefab)
        {
            foreach (var prefab in dataContainer.Departments[departmentType].Prefabs)
            {
                if (namePrefab == prefab.name)
                {
                    return prefab.gameObject;
                }
            }
            return null;
        }

        public List<string> GetSavedDepartment(DepartmentType departmentType)
        {
            if (PlayerPrefs.HasKey(departmentType.ToString()))
                return JsonUtility.FromJson<DepartmentSavePrefs>(PlayerPrefs.GetString(departmentType.ToString())).PrefabsName;
            return null;
        }

    }
}
