﻿using AceCream.Scripts.Template.Effects;
using Atelier.Scripts;
using UnityEngine;

namespace AceCream.Scripts
{
    public class GameCamera : MonoBehaviour
    {
        public Camera Camera { get; private set; }
        public CameraMatrixEffect MatrixEffect;

        private void Awake()
        {
            Camera = GetComponent<Camera>();
            MatrixEffect = GetComponentInChildren<CameraMatrixEffect>();
            if (GameController.Instance.GameCamera.Camera != Camera)
            {
                Destroy(gameObject);
            }
        }
        
        public (Vector2 upperLeft, Vector2 upperRight, Vector2 lowerLeft, Vector2 lowerRight) 
            GetScreenCornersWorldPosition()
        {
            Camera = GetComponent<Camera>();
            var depth = 1f; 
            var upperLeftScreen =  new Vector3(0, Screen.height, depth );
            var upperRightScreen = new Vector3(Screen.width, Screen.height, depth);
            var lowerLeftScreen = new  Vector3(0, 0, depth);
            var lowerRightScreen = new  Vector3(Screen.width, 0, depth);
   
        
            var upperLeft = Camera.ScreenToWorldPoint(upperLeftScreen);
            var upperRight = Camera.ScreenToWorldPoint(upperRightScreen);
            var lowerLeft = Camera.ScreenToWorldPoint(lowerLeftScreen);
            var lowerRight = Camera.ScreenToWorldPoint(lowerRightScreen);

            return (upperLeft, upperRight, lowerLeft, lowerRight);
        }
    }
}
