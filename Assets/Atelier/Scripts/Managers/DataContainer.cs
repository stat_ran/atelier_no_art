﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "DataContainer", menuName = "DataContainer", order = 51)]
public class DataContainer : ScriptableObject
{
    public GameObject FinishStagePrefab;
    public GameObject PhotoFramePrefab;
    public IconChangeModel IconChangeModel;
    public Pers[] Perses;
    public Color[] Colors;
    
    public Hat    Hat;
    public Puffer Puffer;
    public Scarf  Scarf;
    public Shoes  Shoes;
    public Bottom Bottom;
    public Dress  Dress;
    public Upper  Upper;
    
    private Dictionary<DepartmentType, DepartmentData> _data=new Dictionary<DepartmentType, DepartmentData>();
    
    public Dictionary<DepartmentType, DepartmentData> Departments
    {
        get
        {
            if (_data.Count != 0) return _data;

            _data[DepartmentType.Bottom] = Bottom;
            _data[DepartmentType.Dress] = Dress;
            _data[DepartmentType.Hat] = Hat;
            _data[DepartmentType.Puffer] = Puffer;
            _data[DepartmentType.Scarf] = Scarf;
            _data[DepartmentType.Shoes] = Shoes;
            _data[DepartmentType.Upper] = Upper;
            
            return _data;
        }
    }
}

[System.Serializable]
public class DepartmentData
{
    public DepartmentType departmentType;
    public BaseClothesPrefab[] Prefabs;
    public Decorses[] Decorses;
    public Sprite ColorSprite;
}

[System.Serializable]
public class Hat : DepartmentData
{
    
}

[System.Serializable]
public class Puffer : DepartmentData
{
    
}

[System.Serializable]
public class Bottom : DepartmentData
{
    
}

[System.Serializable]
public class Scarf : DepartmentData
{
    
}

[System.Serializable]
public class Dress : DepartmentData
{
   
}

[System.Serializable]
public class Upper : DepartmentData
{
    
}

[System.Serializable]
public class Shoes : DepartmentData
{
    
}


[System.Serializable]
public class Decor
{
    public Sprite Tap;
    public Sprite[] Sprites;
    public int Number;

    public Sprite GetTap()
    {
        if (Tap == null) return Sprites[0];
        return Tap;
    }
}

[System.Serializable]
public class Decorses
{
    public Decor[] Decors;
}
