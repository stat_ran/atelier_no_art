﻿using UnityEngine;

public class LevelController : MonoBehaviour
{

    public GameObject PrefabGameObject;

    [HideInInspector] public Vector2 CursorPosition;

    private Camera _camera;

    public void Awake()
    {
        _camera=Camera.main;
    }
    
    public void UpdateCursor()
    {
        CursorPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
    }

    private void Update()
    {
        UpdateCursor();

        if (Input.GetMouseButton(0))
        {
            
        }
       
        if (Input.GetMouseButtonDown(0))
        {
            
        }

        if (Input.GetMouseButtonUp(0))
        {
           
        }
    }

    // private void ReportStartLevel()
    // {
    //     Dictionary<string, object> dictionary=new Dictionary<string, object>();
    //     dictionary.Add("level", CurrentCharacterClass.Name.ToString());
    //     AppMetrica.Instance.ReportEvent("level_start", dictionary);
    //     AppMetrica.Instance.SendEventsBuffer();
    //     _startTime = Time.time;
    // }
    //
    // private void ReportFinishLevel()
    // {
    //     int t = (int)(Time.time - _startTime);
    //     Dictionary<string, object> dictionary=new Dictionary<string, object>();
    //     dictionary.Add("level", CurrentCharacterClass.Name.ToString());
    //     dictionary.Add("time", t);
    //     AppMetrica.Instance.ReportEvent("level_finish", dictionary);
    //     AppMetrica.Instance.SendEventsBuffer();
    // }
    
    

}
