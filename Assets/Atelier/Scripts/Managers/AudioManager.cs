﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [HideInInspector] public AudioSource AudioSourceMusic;

    private const string SoundVolume = "SoundVolume";
    private const string MusicVolume = "MusicVolume";
    private const string IsPushEvent = "IsPushEvent";
    

    void Awake()
    {
        if (Instance == null) Instance = this;
        Init();
        DontDestroyOnLoad(this);
    }

    void Init()
    {
        if (!PlayerPrefs.HasKey(SoundVolume)) PlayerPrefs.SetFloat(SoundVolume, 1f);
        if (!PlayerPrefs.HasKey(MusicVolume)) PlayerPrefs.SetFloat(MusicVolume, 1f);
        if (!PlayerPrefs.HasKey(IsPushEvent)) PlayerPrefs.SetInt(IsPushEvent, 1);

        AudioSourceMusic = GetComponent<AudioSource>();
    }

    public void PlaySound(AudioClip clip)
    {
        if (PlayerPrefs.GetFloat(SoundVolume) <= 0.0f) return;

        AudioSourceMusic.PlayOneShot(clip, PlayerPrefs.GetFloat(SoundVolume));
    }

    public void PlayMusic(AudioClip clip, AudioSource audioSource)
    {
        if (PlayerPrefs.GetFloat(MusicVolume) <= 0.0f) return;
        if (audioSource == null) audioSource = AudioSourceMusic;

        audioSource.loop = true;
        audioSource.clip = clip;
        audioSource.volume = PlayerPrefs.GetFloat(MusicVolume);
        audioSource.Play();
    }

    public void StopMusic(AudioSource audioSource)
    {
        audioSource.Stop();
    }

    public void MuteSound()
    {
        bool enabled = PlayerPrefs.GetFloat(SoundVolume) > 0.0f;
        PlayerPrefs.SetFloat(SoundVolume, enabled ? 0.0f : 1.0f);
        PlayerPrefs.Save();
    }

    public void MuteMusic()
    {
        bool enabled = PlayerPrefs.GetFloat(MusicVolume) > 0.0f;
        PlayerPrefs.SetFloat(MusicVolume, enabled ? 0.0f : 1.0f);
        PlayerPrefs.Save();
        AudioSourceMusic.Stop();
    }
    
}