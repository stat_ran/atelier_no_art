﻿using UnityEngine;

public class InputObject : MonoBehaviour
{
    [HideInInspector] public Vector2 FirstPosition;
    [HideInInspector] public Vector2 SwipeDirection;
    [HideInInspector] public bool IsDown;
    [HideInInspector] public bool IsMovementAllowed = true;
    [HideInInspector] public Vector2 PrevPos;
    [HideInInspector] public Vector2 FirstGlobalPosition;
    private bool _isFirstTouch = false;
    [HideInInspector] public Transform Transform;
    private Camera _camera;

    public virtual void Start()
    {
        Transform = transform;
        _camera=Camera.main;
    }

    private void OnMouseDown()
    {
#if !UNITY_EDITOR
         if (Input.touchCount != 1) return;
         PrevPos = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);
#endif
#if UNITY_EDITOR
        PrevPos = _camera.ScreenToWorldPoint(Input.mousePosition);
#endif
       
        FirstPosition = Transform.localPosition;
        FirstGlobalPosition = Transform.position;
        IsDown = true;
        _isFirstTouch = true;
        SwipeDirection = Vector2.zero;
        NotificationCenter.postNotification("Push_Item", Transform);
        TouchDown();
    }

    private void OnMouseDrag()
    {
        if (_isFirstTouch)
        {
#if !UNITY_EDITOR
            Vector2 touchPos = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);
            SwipeDirection = Input.GetTouch(0).deltaPosition;
#endif
#if UNITY_EDITOR
            Vector2 touchPos = _camera.ScreenToWorldPoint(Input.mousePosition);
            SwipeDirection = touchPos - PrevPos;
#endif
            
            if (IsMovementAllowed)
                Transform.position = new Vector3(touchPos.x, touchPos.y, Transform.position.z);

            PrevPos = touchPos;
            NotificationCenter.postNotification("Move_Item", Transform);
            TouchDrag();
        }
    }

    private void OnMouseUp()
    {
        if (_isFirstTouch)
        {
            IsDown = false;
            _isFirstTouch = false;
            SwipeDirection = Vector2.zero;
            PrevPos = Vector2.zero;
            NotificationCenter.postNotification("Pull_Item", Transform);
            TouchUp();
        }
    }

    public virtual void TouchUp()
    {
        
    }

    public virtual void TouchDown()
    {
        
    }
    
    public virtual void TouchDrag()
    {
        
    }

}
