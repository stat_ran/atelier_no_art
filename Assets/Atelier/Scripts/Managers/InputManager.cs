﻿using System;
using System.Collections.Generic;
using System.Linq;
using AceCream.Scripts;
using UnityEngine;

namespace Atelier.Scripts
{
    public class InputManager: UnitySingleton<InputManager>
    {
        public const float CLICK_THRESHOLD = .1f;
        
        public static event Action<bool> InputLocked;
        
        private static bool _inputLocked;
        public static GameObject LastGameObject { get; private set; }
        public static bool Clickable => Input.touchCount <= 0;
        
        public static void Lock(bool val)
        {
            _inputLocked = val;
            InputLocked?.Invoke(_inputLocked);
        }
        
        public static Vector2 GetWorldPoint()
        {
            return GameController.Instance.GameCamera.Camera.ScreenToWorldPoint(Input.mousePosition);
        }

        private List<BaseBehavior> _selectedItems;
        private void Update()
        {
            return;
            // if (GameController.Instance.State != GameController.GameState.Run) return;
            //
            // if (Input.GetMouseButtonDown(0))
            // {
            //     _selectedItems = Raycast2DBehaviour();
            //     if (_selectedItems == null) return;
            //     foreach (var item in _selectedItems)
            //     {
            //         item.OnMouseButtonDown2D(_selectedItems);
            //     }
            // }
            // else if (Input.GetMouseButtonUp(0))
            // {
            //     var add = Raycast2DBehaviour();
            //     if(add != null) _selectedItems.AddRange(add);
            //     if (_selectedItems == null) return;
            //     foreach (var item in _selectedItems)
            //     {
            //         item.OnMouseButtonUp2D(_selectedItems);
            //     }
            //
            //     _selectedItems = null;
            // }
            //
            // _selectedItems?.ForEach(i => i?.OnMouseMove(GetWorldPoint()));
        }

        private List<BaseBehavior> Raycast2DBehaviour()
        {
            List<BaseBehavior> result = null;
            var hit = Physics2D.Raycast(GameController.Instance.GameCamera.Camera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.transform != null)
            {
                LastGameObject = hit.transform.gameObject;
                result = hit.transform.GetComponents<BaseBehavior>().ToList();
            }

            return result;
        }
    }
}