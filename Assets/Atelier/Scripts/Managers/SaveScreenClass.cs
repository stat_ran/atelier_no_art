﻿using System.Collections;
using System.Collections.Generic;
using Atelier.Scripts;
using UnityEngine;

public class SaveScreenClass : BaseBehavior
{
    [HideInInspector] public SaveScreenJson SaveScreenJson;
    private const string saveNameJson = "ScreenJson";

    public override void Awake()
    {
        base.Awake();
        if (PlayerPrefs.HasKey(saveNameJson))
        {
            SaveScreenJson = JsonUtility.FromJson<SaveScreenJson>(PlayerPrefs.GetString(saveNameJson));
        }
        else
        {
            SaveScreenJson=new SaveScreenJson();
        }
    }
}

[System.Serializable]
public class SaveScreenJson
{
    public List<JsonPuffer> Puffer;
    public List<JsonScarf> Scarf;
    public List<JsonDress> Dress;
    public List<JsonHat> Hat;
    public List<JsonBottom> Bottom;
    public List<JsonShoes> Shoese;
    public List<JsonUpper> Upper;

    public SaveScreenJson()
    {
        Puffer=new List<JsonPuffer>();
        Scarf=new List<JsonScarf>();
        Dress=new List<JsonDress>();
        Hat=new List<JsonHat>();
        Bottom=new List<JsonBottom>();
        Shoese=new List<JsonShoes>();
        Upper=new List<JsonUpper>();
    }
}

[System.Serializable]
public class JsonPuffer
{
    public int NumberPrefab;
    public Color Color;
    public int Decor1;
    public int Decor2;
}

[System.Serializable]
public class JsonDress
{
    public int NumberPrefab;
    public Color Color;
    public int Decor1;
}

[System.Serializable]
public class JsonHat
{
    public int NumberPrefab;
    public int Decor1;
    public int Decor2;
}

[System.Serializable]
public class JsonUpper
{
    public int NumberPrefab;
    public Color Color;
    public int Decor1;
}

[System.Serializable]
public class JsonBottom
{
    public int NumberPrefab;
    public Color Color;
    public int Decor1;
    public int Decor2;
}

[System.Serializable]
public class JsonShoes
{
    public int NumberPrefab;
    public Color Color;
    public int Decor1;
}

[System.Serializable]
public class JsonScarf
{
    public int NumberPrefab;
    public int Decor1;
    public int Decor2;
}

