﻿using AceCream.Scripts;
using UnityEngine;

namespace Atelier.Scripts
{
    public class SnapToCorner : BaseBehavior
    {
        [SerializeField] private Direction _direction;
        [SerializeField] private bool _update;

        public override void Awake()
        {
            base.Awake();
            Apply();
        }

        public override void Start()
        {
            base.Start();
            
        }
        
        private void Update()
        {
           if(_update) Apply();
        }
        
        private void Apply()
        {
            switch (_direction)
            {
                case Direction.Left:
                    transform.position = Camera.GetScreenCornersWorldPosition().lowerLeft;
                    break;
                case Direction.Right:
                    transform.position = Camera.GetScreenCornersWorldPosition().lowerRight;
                    break;
            }
        }

        [ContextMenu("SetToLowerLeft")]
        private void SetToLowerLeft()
        {
            var position = Camera.GetScreenCornersWorldPosition().lowerLeft;
            transform.position = position;
        }

        [ContextMenu("SetToLowerRight")]
        public void SetToLowerRight()
        {
            var position = Camera.GetScreenCornersWorldPosition().lowerRight;
            transform.position = position;
        }
    }
}
